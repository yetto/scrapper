# Create cert
sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/nginx-selfsigned.key -out /etc/ssl/certs/nginx-selfsigned.crt

# Output
# Country Name (2 letter code) [AU]:US
# State or Province Name (full name) [Some-State]:New York
# Locality Name (eg, city) []:New York City
# Organization Name (eg, company) [Internet Widgits Pty Ltd]:Bouncy Castles, Inc.
# Organizational Unit Name (eg, section) []:Ministry of Water Slides
# Common Name (e.g. server FQDN or YOUR name) []:server_IP_address
# Email Address []:admin@your_domain.com

# The most important line is the one that requests the Common Name (e.g. server FQDN or YOUR name). You need to enter the domain name associated with your server or, more likely, your server's public IP address.
# Ej: 34.213.83.61

# Configure self signed cert
sudo openssl dhparam -out /etc/ssl/certs/dhparam.pem 2048

# Nginx cert
sudo vim /etc/nginx/snippets/self-signed.conf

# CONF NGINX
# /etc/nginx/snippets/self-signed.conf
  # ssl_certificate /etc/ssl/certs/nginx-selfsigned.crt;
  # ssl_certificate_key /etc/ssl/private/nginx-selfsigned.key;

# Params
sudo vim /etc/nginx/snippets/ssl-params.conf


# Finally, you should take take a moment to read up on HTTP Strict Transport Security, or HSTS, and specifically about the "preload" functionality. Preloading HSTS provides increased security, but can have far reaching consequences if accidentally enabled or enabled incorrectly. In this guide, we will not preload the settings, but you can modify that if you are sure you understand the implications

#/etc/nginx/snippets/ssl-params.conf
  # # from https://cipherli.st/
  # # and https://raymii.org/s/tutorials/Strong_SSL_Security_On_nginx.html

  # ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
  # ssl_prefer_server_ciphers on;
  # ssl_ciphers "EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH";
  # ssl_ecdh_curve secp384r1;
  # ssl_session_cache shared:SSL:10m;
  # ssl_session_tickets off;
  # ssl_stapling on;
  # ssl_stapling_verify on;
  # resolver 8.8.8.8 8.8.4.4 valid=300s;
  # resolver_timeout 5s;
  # # Disable preloading HSTS for now.  You can use the commented out header line that includes
  # # the "preload" directive if you understand the implications.
  # #add_header Strict-Transport-Security "max-age=63072000; includeSubdomains; preload";
  # add_header Strict-Transport-Security "max-age=63072000; includeSubdomains";
  # add_header X-Frame-Options DENY;
  # add_header X-Content-Type-Options nosniff;

  # ssl_dhparam /etc/ssl/certs/dhparam.pem;



# /etc/nginx/sites-available/default
  # server {
  #   listen 80 default_server;
  #   listen [::]:80 default_server;
  #   server_name server_domain_or_IP;
  #   return 302 https://$server_name$request_uri;
  # }

  # server {

  #     # SSL configuration

  #     listen 443 ssl http2 default_server;
  #     listen [::]:443 ssl http2 default_server;
  #     include snippets/self-signed.conf;
  #     include snippets/ssl-params.conf;

# Then
sudo ufw allow 'Nginx Full'; sudo ufw status; sudo nginx -t

# If everything is successful, you will get a result that looks like this:
  # Output
  # nginx: [warn] "ssl_stapling" ignored, issuer certificate not found
  # nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
  # nginx: configuration file /etc/nginx/nginx.conf test is successful

# Done >
sudo systemctl restart nginx

# Change to a Permanent Redirect
  # Find the return 302 and change it to return 301

sudo nginx -t; sudo systemctl restart nginx