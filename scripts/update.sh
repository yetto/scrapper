#!/bin/bash
cd ~/node/

# Update base dir
cd Caro/
rm ./package-lock.json
npm install
cd ../

# Sync destinations and symlink modules
rsync -avz --exclude=".env" --exclude node_modules/ --progress ./Caro/ ./one/
rm -rf ./one/node_modules
ln -sf ../Caro/node_modules ./one/node_modules

rsync -avz --exclude=".env" --exclude node_modules/ --progress ./Caro/ ./two/
rm -rf ./two/node_modules
ln -sf ../Caro/node_modules ./two/node_modules

rsync -avz --exclude=".env" --exclude node_modules/ --progress ./Caro/ ./three/
rm -rf ./three/node_modules
ln -sf ../Caro/node_modules ./three/node_modules

rsync -avz --exclude=".env" --exclude node_modules/ --progress ./Caro/ ./four/
rm -rf ./four/node_modules
ln -sf ../Caro/node_modules ./four/node_modules