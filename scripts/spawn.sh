#!/bin/bash
cd ~/node/

# Make sure we kill all leftover instances
pm2 delete all

# Span all scrappers
cd one    && pm2 start --interpreter xvfb-run npm --name one -- run debug --watch     && cd ../
cd two    && pm2 start --interpreter xvfb-run npm --name two -- run debug --watch     && cd ../
cd three  && pm2 start --interpreter xvfb-run npm --name three -- run debug --watch   && cd ../
cd four   && pm2 start --interpreter xvfb-run npm --name four -- run debug --watch    && cd ../

# Open logs imediatly
pm2 logs
