#!/bin/bash

###########################
## Vars
###########################
HOST=scrap2
USER=ubuntu

###########################
## Upload & Instalation
###########################
ssh $USER@$HOST 'bash -s' < installation.sh
rsync -avz --progress ./spawn.sh -e "ssh"  $USER@$HOST:/home/$USER/node/
rsync -avz --progress ./update.sh -e "ssh"  $USER@$HOST:/home/$USER/node/

###########################
## Push to repo
###########################
git remote add $HOST $HOST:/home/ubuntu/ci_repos/caro.git
git push $HOST staging

###########################
## Deploy
###########################
ssh $USER@$HOST 'bash -s' < deploy.sh

