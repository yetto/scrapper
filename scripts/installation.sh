#!/bin/bash

##########################
## Basics
##########################

# Step one server and setup
# NGINX
sudo apt-get update; sudo apt-get install nginx -y; sudo ufw allow 'Nginx Full'; sudo ufw status; systemctl status nginx &&

# NGINX config files
sudo cp /etc/nginx/sites-available/default /etc/nginx/sites-available/caro;
sudo ln -s /etc/nginx/sites-available/caro /etc/nginx/sites-enabled/;

# MAIN Scrapper config file
sudo cat <<EOT >> /etc/nginx/sites-available/caro
server {
    listen 80;
    server_name example.com;
    location / {
      return 301 https://www.google.com/;
    }
    location /one {
        proxy_pass http://localhost:5051;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
    }
    location /two {
      proxy_pass http://localhost:5052;
      proxy_http_version 1.1;
      proxy_set_header Upgrade $http_upgrade;
      proxy_set_header Connection 'upgrade';
      proxy_set_header Host $host;
      proxy_cache_bypass $http_upgrade;
    }
    location /three {
      proxy_pass http://localhost:5053;
      proxy_http_version 1.1;
      proxy_set_header Upgrade $http_upgrade;
      proxy_set_header Connection 'upgrade';
      proxy_set_header Host $host;
      proxy_cache_bypass $http_upgrade;
    }
    location /four {
      proxy_pass http://localhost:5054;
      proxy_http_version 1.1;
      proxy_set_header Upgrade $http_upgrade;
      proxy_set_header Connection 'upgrade';
      proxy_set_header Host $host;
      proxy_cache_bypass $http_upgrade;
    }
}
EOT


###########################
## Enviroment
###########################

# Step two enviroment
sudo apt-get update; sudo apt-get install -y build-essential libssl-dev; curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.2/install.sh | bash; source ~/.bashrc;
export NVM_DIR="$HOME/.nvm";
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh";  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion";
nvm install 6.11.2; nvm use 6.11.2;

cd ~; mkdir ci_repos; cd ci_repos; mkdir caro.git &&
cd /home/ubuntu/ci_repos/caro.git && git init --bare &&
cd ~; touch /home/ubuntu/ci_repos/caro.git/hooks/post-receive &&
chmod +x /home/ubuntu/ci_repos/caro.git/hooks/post-receive &&

HOOK_PATH=/home/ubuntu/ci_repos/caro.git/hooks/post-receive
echo '#!/bin/sh' >> $HOOK_PATH
echo 'git --work-tree=/home/ubuntu/node/Caro --git-dir=/home/ubuntu/ci_repos/caro.git checkout staging -f' >> $HOOK_PATH

mkdir /home/ubuntu/node && mkdir /home/ubuntu/node/Caro;

## NODE JS GLOBAL PACKAGES
npm i -g pm2; pm2 list;

# INSTALL dependencies for headless
sudo apt-get install xvfb -y