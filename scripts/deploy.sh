#!/bin/bash

# Load NVM so we can use npm
export NVM_DIR="$HOME/.nvm";
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh";  # This loads nvm

###########################
## Vars
###########################
HOST=scrap2
USER=ubuntu

###########################
## Cloning
###########################

# Creates scrapper clones
cd ~/node;
mkdir one;
mkdir two;
mkdir three;
mkdir four;

# Installs node dependencies
cd Caro; rm package-lcok.json; npm install; cd ../;

# sync all clones
rsync -avz --exclude=".env" --exclude node_modules/ --progress ./Caro/ ./one/
rm -rf ./one/node_modules
ln -sf ../Caro/node_modules ./one/node_modules

rsync -avz --exclude=".env" --exclude node_modules/ --progress ./Caro/ ./two/
rm -rf ./two/node_modules
ln -sf ../Caro/node_modules ./two/node_modules

rsync -avz --exclude=".env" --exclude node_modules/ --progress ./Caro/ ./three/
rm -rf ./three/node_modules
ln -sf ../Caro/node_modules ./three/node_modules

rsync -avz --exclude=".env" --exclude node_modules/ --progress ./Caro/ ./four/
rm -rf ./four/node_modules
ln -sf ../Caro/node_modules ./four/node_modules


# Scrappers env files

touch /home/$USER/node/one/.env;
cat <<EOT >> /home/$USER/node/one/.env
PORT=5051
NODE_ENV=staging
PER_PAGE=10
SECRET=~ck')_GU.]aWQ{JgNHc~z<6}Vk7wDkB4zJ$Q!MFc
SECRET_HINT=~ coffee korean ' ) _ GOLF USA . ] apple WALMART QUEEN { JACK golf NUT HULU coffee ~ zip < 6 } VISA korean 7 walmart DRIP korean BESTBUY 4 zip JACK $ QUEEN ! MUSIC FRUIT coffee
JARVIS_DEV=http://localhost:5050
JARVIS_STG=http://34.210.203.130:5050
JARVIS_PROD=http://jarvis.yaydoo.com
INTERFACE=127.0.0.1
SELF_LOCATION=http://localhost
_SELF=5452bbc1-ed26-5e17-ae14-cbcacf05e2bb
PROXY_MAIN=http://34.210.249.224:81
PROXY_INT3=http://163.172.48.109:15001,http://163.173.36.181:15001
PROXY_INT15=http://63.141.241.98:16001,http://163.172.36.211:16001
BROWSER_WINDOW=false
EOT

touch /home/$USER/node/two/.env;
cat <<EOT >> /home/$USER/node/two/.env
PORT=5052
NODE_ENV=staging
PER_PAGE=10
SECRET=~ck')_GU.]aWQ{JgNHc~z<6}Vk7wDkB4zJ$Q!MFc
SECRET_HINT=~ coffee korean ' ) _ GOLF USA . ] apple WALMART QUEEN { JACK golf NUT HULU coffee ~ zip < 6 } VISA korean 7 walmart DRIP korean BESTBUY 4 zip JACK $ QUEEN ! MUSIC FRUIT coffee
JARVIS_DEV=http://localhost:5050
JARVIS_STG=http://34.210.203.130:5050
JARVIS_PROD=http://jarvis.yaydoo.com
INTERFACE=127.0.0.1
SELF_LOCATION=http://localhost
_SELF=5452bbc1-ed26-5e17-ae14-cbcacf05e2bb
PROXY_MAIN=http://34.210.249.224:81
PROXY_INT3=http://163.172.48.109:15001,http://163.173.36.181:15001
PROXY_INT15=http://63.141.241.98:16001,http://163.172.36.211:16001
BROWSER_WINDOW=false
EOT

touch /home/$USER/node/three/.env;
cat <<EOT >> /home/$USER/node/three/.env
PORT=5053
NODE_ENV=staging
PER_PAGE=10
SECRET=~ck')_GU.]aWQ{JgNHc~z<6}Vk7wDkB4zJ$Q!MFc
SECRET_HINT=~ coffee korean ' ) _ GOLF USA . ] apple WALMART QUEEN { JACK golf NUT HULU coffee ~ zip < 6 } VISA korean 7 walmart DRIP korean BESTBUY 4 zip JACK $ QUEEN ! MUSIC FRUIT coffee
JARVIS_DEV=http://localhost:5050
JARVIS_STG=http://34.210.203.130:5050
JARVIS_PROD=http://jarvis.yaydoo.com
INTERFACE=127.0.0.1
SELF_LOCATION=http://localhost
_SELF=5452bbc1-ed26-5e17-ae14-cbcacf05e2bb
PROXY_MAIN=http://34.210.249.224:81
PROXY_INT3=http://163.172.48.109:15001,http://163.173.36.181:15001
PROXY_INT15=http://63.141.241.98:16001,http://163.172.36.211:16001
BROWSER_WINDOW=false
EOT

touch /home/$USER/node/four/.env;
cat <<EOT >> /home/$USER/node/four/.env
PORT=5054
NODE_ENV=staging
PER_PAGE=10
SECRET=~ck')_GU.]aWQ{JgNHc~z<6}Vk7wDkB4zJ$Q!MFc
SECRET_HINT=~ coffee korean ' ) _ GOLF USA . ] apple WALMART QUEEN { JACK golf NUT HULU coffee ~ zip < 6 } VISA korean 7 walmart DRIP korean BESTBUY 4 zip JACK $ QUEEN ! MUSIC FRUIT coffee
JARVIS_DEV=http://localhost:5050
JARVIS_STG=http://34.210.203.130:5050
JARVIS_PROD=http://jarvis.yaydoo.com
INTERFACE=127.0.0.1
SELF_LOCATION=http://localhost
_SELF=5452bbc1-ed26-5e17-ae14-cbcacf05e2bb
PROXY_MAIN=http://34.210.249.224:81
PROXY_INT3=http://163.172.48.109:15001,http://163.173.36.181:15001
PROXY_INT15=http://63.141.241.98:16001,http://163.172.36.211:16001
BROWSER_WINDOW=false
EOT