
/**
 * Work Order Model
 */

let currentOrder = {
  _id              : null,
  url              : null,
  template         : null,
  templateData     : null,
  connection       : null,
  error            : null,
  scrapperId       : null,
  scrapperLocation : null,
  // 'new', 'error', 'done', 'active'
  status           : null
}; // END currentOrder


let defaultOrder = {};
Object.assign(defaultOrder, currentOrder);


module.exports = {
  default : defaultOrder,
  current : currentOrder
};