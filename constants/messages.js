const
  dotenv = require('dotenv').load(),
  env    = process.env;

/* AUTH MESSAGES */
const AUTH = exports.AUTH = {
  error : {
    default : { message: 'unhandle exception', code: 500 },
    denied  : { message: 'Permission denied', code: 550  },
  }
}; // END Auth Messages

/* ENDPOINS */
const PRODUCT = exports.PRODUCT = {
  error : {
    duplicate : { message: 'unhandle exception', code: 500 },
    ok        : { message: 'Permission denied', code: 200  },
  }
}; // END Auth Messages

// http://www.iana.org/assignments/http-status-codes/http-status-codes.xhtml
// https://httpstatuses.com
const HTTP = exports.HTTP = {
  c200 : {
    code: 200,
    message : "OK"
  },
  c300 : {
    code: 300,
    message : "Multiple Choices"
  },
  c301 : {
    code: 301,
    message : "Moved Permanently"
  },
  c302 : {
    code: 302,
    message : "Found"
  },
  c304 : {
    code: 304,
    message : "Not Modified"
  },
  c307 : {
    code: 307,
    message : "Temporary Redirect"
  },
  c400 : {
    code: 400,
    message : "The server cannot or will not process the request due to something that is perceived to be a client error."
  },
  c401 : {
    code: 401,
    message : "Unauthorized"
  },
  c403 : {
    code: 403,
    message : "Forbidden"
  },
  c404 : {
    code: 404,
    message : "Not Found"
  },
  c410 : {
    code: 410,
    message : "Gone"
  },
  c500 : {
    code: 500,
    message : "Unhandled Exception" // Internal Server Error
  },
  c501 : {
    code: 501,
    message : "Not Implemented"
  },
  c503 : {
    code: 503,
    message : "Service Unavailable"
  },
  c550 : {
    code: 550,
    message : "Permission denied"
  },
} // END http
