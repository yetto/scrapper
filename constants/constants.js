
const
  dotenv = require('dotenv').load(),
  uuidv5 = require('uuid/v5'),
  env    = process.env;

const NODE_ENV = exports.NODE_ENV = env.NODE_ENV;

/* Throw if ambient is not defied */
try {
  if (!NODE_ENV) throw new Error("No ambient defined in .env file. Example: NODE_ENV=development");
} catch(e) {
  console.error(e);
} // END try


/** ##########################
 *  CONSTANTS
 ########################## */
const SELF_LOCATION = exports.SELF_LOCATION = env.SELF_LOCATION;

/* JARVIS lo-Ecation */
let JARVIS_LOCATION;
if      (NODE_ENV === "development") JARVIS_LOCATION = env.JARVIS_DEV
else if (NODE_ENV === "staging")     JARVIS_LOCATION = env.JARVIS_STG
else if (NODE_ENV === "production")  JARVIS_LOCATION = env.JARVIS_PROD;
exports.JARVIS_LOCATION = JARVIS_LOCATION;

let SELF_URL;
if      (NODE_ENV === "development") SELF_URL = SELF_LOCATION + ":" + env.PORT;
else if (NODE_ENV === "staging")     SELF_URL = SELF_LOCATION;
else if (NODE_ENV === "production")  SELF_URL = SELF_LOCATION;
exports.SELF_URL = SELF_URL;

let WS_LOCATION;
if      (NODE_ENV === "staging" || NODE_ENV === "development") WS_LOCATION = env.WS_DEV;
else if (NODE_ENV === "production")                            WS_LOCATION = env.WS_PROD;
exports.WS_LOCATION = WS_LOCATION;

const _SELF = exports._SELF = uuidv5(SELF_URL, uuidv5.URL);
const PROCESS_TITLE = exports.PROCESS_TITLE = "Caro" + env.PORT;
const RATE_LIMITS = exports.RATE_LIMITS = {
  tick         : 120000,
  shortTick    : 5000,
  howMany      : 1000,
  every        : "minute",
  timeThrottle : 1000,
  nightmare    : 4000,
  burst        : 500,
  proxyBurst   : 10000,
  nmTimeouts   : {
    waitTimeout      : 25000,
    gotoTimeout      : 25000,
    loadTimeout      : 25000,
    executionTimeout : 25000
  }
} // END RATE_LIMITS
const PERMISSION_SETS = exports.PERMISSION_SETS = {
  world : ["world"],
  all   : ["admin:read", "admin:write", "user:read", "user:write"],
  arw   : ["admin:read", "admin:write"],
  aw    : ["admin:write"],
  ar    : ["admin:read"],
  urw   : ["user:read", "user:write", "admin:read", "admin:write"],
  uw    : ["user:write", "admin:write"],
  ur    : ["user:read", "admin:read"]
} // END PERMISSION_SETS
const JARVIS = exports.JARVIS = {

  // Update this order
  updateScrapeOrder : {
    method   : "PUT",
    endpoint : JARVIS_LOCATION + "/scrapeOrder/ingest/"
  },

  // Register new Crawler
  registerNewCrawler : {
    method   : "POST",
    endpoint : JARVIS_LOCATION + "/crawler/"
  },

  // deRegister
  deRegister : {
    method   : "DELETE",
    endpoint : JARVIS_LOCATION + "/crawler/" + _SELF
  },

  // Update Crawler
  updateCrawler : {
    method   : "PUT",
    endpoint : JARVIS_LOCATION + "/crawler/" // + Crawler UUID
  },

  // Scrapping orders
  workOrder : {
    method   : "POST",
    endpoint : JARVIS_LOCATION + "/scrapeOrder/"
  },
  updateWorkOrder : {
    method   : "PUT",
    endpoint : JARVIS_LOCATION + "/scrapeOrder/ingest"
  },

  // Products
  createProdcut : {
    method   : "POST",
    endpoint : JARVIS_LOCATION + "/product/"
  },
  reportProduct : {
    method   : "PUT",
    endpoint : JARVIS_LOCATION + "/product/ingest"
  },
  reportRemoteInfo: {
    method: "POST",
    endpoint : JARVIS_LOCATION + "/crawler/new"
  },
  subscribe : {
    method   : "POST",
    endpoint : JARVIS_LOCATION + "/subscribe"
  }

} // END JARVIS

const WS = exports.WS = {
  // Update this order
  updatePrice : {
    method   : "PUT",
    endpoint : WS_LOCATION + "/admin/provider/{providerId}/item/{itemId}/price"
  }

} // END WS

const BROWSER_WINDOW = exports.BROWSER_WINDOW = (process.env.BROWSER_WINDOW == 'true');

// Proxies
var PROXIES = exports.PROXIES = {
  main: process.env.PROXY_MAIN.split(","),
  interval3: process.env.PROXY_INT3.split(","),
  interval15: process.env.PROXY_INT15.split(",")

} // END PROXIES
