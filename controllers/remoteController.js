const
  debug         = require('./libs/debugConf').log('scrape:controller'),
  knowException = require('../libs/knowExceptions.js'),
  dcm           = require('../templates/dcm.js'),
  Promise       = require("bluebird");


/**
 * scrapeController.js
 * @description :: Server-side logic for managing scrapping.
 */

let despawn = () => {
  debug("despawn");
} // END info

let stats = () => {
  debug("stats");
} // END stats


module.exports = {
  info : info,
  newJob : newJob,
  queueJob : queueJob
};