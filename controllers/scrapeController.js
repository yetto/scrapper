const
  debug         = require('../libs/debugConf').log('scrape:controller'),
  debugErr      = require('../libs/debugConf').err('scrape:controller'),
  Promise       = require("bluebird"),
  knowException = require('../libs/knowExceptions.js'),
  dcm           = require('../templates/dcm.js'),
  corner        = require('../templates/corner.js');


let providers = {
  dcm : dcm,
  corner : corner
};


/**
 * scrapeController.js
 * @description :: Server-side logic for managing scrapping.
 */

/**
 * scrapeController.newOrder()
 */
var parseOrder = exports.parseOrder = (workOrder) => {
  return new Promise(function(resolve, reject){
    try {
      let provider = workOrder.template.split("_")[0],
          name     = workOrder.template,
          data     = workOrder.templateData;

      debug(" --*-- Work Order:", workOrder._id, " provider:", provider, " name:", name);

      providers[provider][name](data)
        .then(data   => resolve(data))
        .catch(error => reject(error));

    } catch(error) {
      debugErr(" --*-- Parse Order > Error: ", error.message);
      error => reject("Work Order is not valid");

    } // END try

  }) // END Promise

} // END newOrder



/** ########################################################
 *  # scrapeController special routes
 ######################################################## */

let despawn = () => {
  debug("despawn");
} // END info


/**
 * scrapeController.stats()
 */
var stats = exports.stats = (orderId) => {
  debug(" --*-- Stats: ",orderId);
  return new Promise(function(resolve, reject){
    dcm
      .progressReport()
      .then(progress => {
        if   (!progress) res.status(200).json({ message: 'No results' });
        else res.status(200).json(progress);
        debug('progress: ',progress);
        resolve(data);
      })
      .catch((error) => reject(error));

  }); // END Promise

} // END stats
