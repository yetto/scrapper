
const
  debug     = require('../libs/debugConf').log('remote:report'),
  debugErr  = require('../libs/debugConf').err('remote:report'),
  validator = require('validator'),
  Promise   = require('bluebird'),
  request   = require('request-promise'),
  // Constants
  { RATE_LIMITS, JARVIS } = require('../constants/constants');

var reportRemoteInfo = exports.prodcut = function(remote){
  debug("Reporting Prodcut: ",remote.providerSku);
  return new Promise(function(resolve, reject){

    let options = {
      method: JARVIS.reportRemoteInfo.method,
      uri: JARVIS.reportRemoteInfo.endpoint,
      body: remote,
      json: true
    };

    request(options)
      .then(parsedBody => resolve(parsedBody))
      .catch(error     => reject(error));

  }); // END promise

} // END reportRemoteInfo