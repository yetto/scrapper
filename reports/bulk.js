
const
  debug         = require('../libs/debugConf').log('bulk:report'),
  debugErr      = require('../libs/debugConf').err('bulk:report'),
  validator = require('validator'),
  Promise   = require('bluebird'),
  request   = require('request-promise'),
  co        = require("co"),
  // Constants
  { RATE_LIMITS,
    JARVIS,
    SELF_URL,
    _SELF } = require('../constants/constants');;



let defaultOrder = {
  _id              : null,
  url              : null,
  template         : null,
  templateData     : null,
  connection       : null,
  error            : null,
  scrapperId       : null,
  scrapperLocation : null,
  // 'new', 'error', 'done', 'active'
  status           : null
}; // END currentOrder


var defaultProduct = {
  name        : null,
  description : null,
  price       : null,
  brand       : null,
  provider    : null,
  providerSku : null,
  productUpc  : null,
  globalSku   : null,
  partNumber  : null,
  images      : null,
  attributes  : null,
  currency    : null,
  category    : null,
  subCategory : null,
  stock       : null,
  _scrapeDate : null,
  _lastStatus : null,
  _sourceUrl  : null
}; // END defaultOrder


/**
 * orders
 * Reports all found orders found
 */
var orders = exports.orders = function(orders){
  debug(" ~~~ Reporting orders > orders ", orders.length);
  return new Promise(function(resolve, reject){

    function *reportCategories() {
      let index = 0;
      while(index <= orders.length - 1){
        debug(" ~~~ Reporting: ",orders[index].url," > #",index);
        yield new Promise(resolve => {
          reportOneOrder(orders[index]);
          setTimeout(resolve, RATE_LIMITS.burst);
        });
        index++;

      } // END while

    } // END reportCategories

    co(reportCategories())
      .then(result => resolve(result))
      .catch(error => reject(error));

    function reportOneOrder(orderObj){

      let newOrder = {};
      Object.assign(newOrder, defaultOrder, orderObj);

      let options = {
        method : JARVIS.workOrder.method,
        uri    : JARVIS.workOrder.endpoint,
        body   : newOrder,
        json   : true
      }; // END option

// debug("options.body",options.body);

      return request(options)
        .then(parsedBody => debug(" ~~~ Report SEND"))
        .catch(error     => debug(" ~~~ Report SEND > error",error.message));

    } // END reportOneOrder

  }); // END promise

} // END orders





/**
 * products
 * Reports all found products found
 */
var products = exports.products = function(products){
  debug(" ~~~ Reporting products > products ", products.length);
  return new Promise(function(resolve, reject){

    function *reportCategories() {
      let index = 0;
      while(index <= products.length - 1){
        debug(" ~~~ Product: ",products[index].providerSku," > #",index);
        yield new Promise(resolve => {
          reportOneProduct(products[index]);
          setTimeout(resolve, RATE_LIMITS.burst);
        });
        index++;

      } // END while

    } // END reportCategories

    co(reportCategories())
      .then(result => resolve(result))
      .catch(error => reject(error));

    function reportOneProduct(productObj){
      let newProduct = {};
      Object.assign(newProduct, defaultProduct, productObj);

      let options = {
        method : JARVIS.reportProduct.method,
        uri    : JARVIS.reportProduct.endpoint,
        body   : newProduct,
        json   : true
      };

      return request(options)
        .then(parsedBody => debug(" ~~~ Report SEND"))
        .catch(error     => debug(" ~~~ Report SEND > error",error.message));

    } // END reportOne

  }); // END promise

} // END products