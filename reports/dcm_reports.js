
const
  debug     = require('../libs/debugConf').log('dcm:report'),
  debugErr  = require('../libs/debugConf').err('dcm:report'),
  validator = require('validator'),
  Promise   = require('bluebird'),
  request   = require('request-promise'),
  co        = require("co"),
  // Constants
  { RATE_LIMITS,
    JARVIS,
    SELF_URL,
    _SELF } = require('../constants/constants');;



let defaultOrder = {
  _id              : null,
  url              : null,
  template         : null,
  templateData     : null,
  connection       : null,
  error            : null,
  scrapperId       : null,
  scrapperLocation : null,
  // 'new', 'error', 'done', 'active'
  status           : null
}; // END currentOrder



/**
 * dcmDiscovery
 * Reports all found categories found on DCM homepage
 */
var dcmDiscovery = exports.dcmDiscovery = function(siteCategories){
  debug(" ~~~ Reporting dcmDiscovery > siteCategories ", siteCategories.length);
  return new Promise(function(resolve, reject){

    function *reportCategories() {
      let index = 0;
      while(index + 1 <= siteCategories.length){
        debug(" ~~~ DCM Discovery: ",siteCategories[index].childCategory," > #",index);
        yield new Promise(resolve => {
          reportOne(siteCategories[index]);
          setTimeout(resolve, RATE_LIMITS.burst);
        });
        index++;

      } // END while

    } // END reportCategories

    co(reportCategories())
      .then(() => debug("Reports done"))
      .catch(error => debugErr(error));

    function reportOne(categorieObj){
      let newOrder = {};
      Object.assign(newOrder, defaultOrder);

      newOrder = {
        url          : categorieObj.url,
        template     : "dcm_category",
        templateData : [categorieObj],
      }; // END currentOrder

      let options = {
        method : JARVIS.workOrder.method,
        uri    : JARVIS.workOrder.endpoint,
        body   : newOrder,
        json   : true
      }; // END option

      request(options)
        .then(parsedBody => resolve(parsedBody))
        .catch(error     => reject(error));

    } // END reportOne

  }); // END promise

} // END dcmDiscovery




/**
 * dcmCategory
 * Reports all product pages found on DCM
 */
var dcmCategory = exports.dcmCategory = function(productPages){
  debug(" ~~~ Reporting dcmCategory: ", productPages);
  return new Promise(function(resolve, reject){

    let options = {
      method : JARVIS.workOrder.method,
      uri    : JARVIS.workOrder.endpoint,
      body   : productPages,
      json   : true
    };

    // request(options)
    //   .then(parsedBody => resolve(parsedBody))
    //   .catch(error     => reject(error));

  }); // END promise

} // END dcmCategory



/**
 * reportProductsInfo
 * Reports  product info found on DCM
 */
var reportProductsInfo = exports.reportProductsInfo = function(product){
  debug(" ~~~ Reporting reportProductsInfo: ");
  debug(product);
  debug(" ~~~ END > Reporting reportProductsInfo");

  return new Promise(function(resolve, reject){

    let options = {
      method : JARVIS.workOrder.method,
      uri    : JARVIS.workOrder.endpoint,
      body   : product,
      json   : true
    };

    // request(options)
    //   .then(parsedBody => resolve(parsedBody))
    //   .catch(error     => reject(error));

  }); // END promise

} // END reportProductsInfo