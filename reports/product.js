
const
  debug     = require('../libs/debugConf').log('product:report'),
  debugErr  = require('../libs/debugConf').err('product:report'),
  validator = require('validator'),
  Promise   = require('bluebird'),
  request   = require('request-promise'),
  // libs
  { parseUrl } = require('../libs/utils.js'),
  {
    Success,
    Failure,
    TryAgain,
    NotFound,
    TemporarilyOffline,
    InvalidData

  } = require('../libs/compseReports'),
  answers = require('../libs/answers'),
  // Constants
  { RATE_LIMITS, JARVIS, WS } = require('../constants/constants');

module.exports = function(product){
  if      (!!product.code === "2") ReportTemplate = NotFound;
  else if (!!product.code === "3") ReportTemplate = TemporarilyOffline;
  else if (!!product.code === "4") ReportTemplate = TryAgain;
  // else if (!product.code) ReportTemplate = Failure;
  else if (!price || !providerSku || !productUpc) {
    product.code === "5";
    ReportTemplate = InvalidData;

  }; // END if

  let { providerSku, productUpc, price } = product
        ReportTemplate                   = Success
        code                             = "prre" + product.code;

  debug(" ~~~ Reporting Prodcut > " + ReportTemplate.name + " > CODE: " + code + " > SKU/UPC: ", providerSku + " / " + productUpc ," PRICE: ", price);

  let options = {
    method  : JARVIS.reportProduct.method,
    uri     : JARVIS.reportProduct.endpoint,
    body    : new ReportTemplate(code, null, product),
    json    : true

  }; // END options

  return request(options);

} // END reportProduct