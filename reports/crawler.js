
const
  debug     = require('../libs/debugConf').log('crawler:report'),
  debugErr  = require('../libs/debugConf').err('crawler:report'),
  validator = require('validator'),
  Promise   = require('bluebird'),
  request   = require('request-promise'),
  // Constants
  { RATE_LIMITS,
    JARVIS,
    _SELF,
    SELF_URL } = require('../constants/constants');



/**
 * Updates crawler status
 * @status : is the scrapper busy?
 */
var updateCrawler = exports.updateCrawler = function(status){

  let self_report_req_options = {
    method : "POST",
    uri    : SELF_URL + "/scrape/status",
    body   : { "busy": status || false },
    json   : true

  };

  let jarvis_report_req_options = {
    method : JARVIS.updateCrawler.method,
    uri    : JARVIS.updateCrawler.endpoint + _SELF,
    body   : { "busy": status || false },
    json   : true

  };

  return request(self_report_req_options)
    .then(parsedBody => request(jarvis_report_req_options));

} // END updateCrawler



/**
 * Sets self as not busy
 */
var notBusy = exports.notBusy = function(){

  let options = {
    method : "GET",
    uri    : SELF_URL + "/scrape/status/notbusy",
    json   : true
  };

  request(options)
    .then(parsedBody => debug(" ~~~ Set as not busy 2: ", parsedBody.busy))
    .catch(error     => debug(error));

} // END notBusy




/**
 * Report with jarvis on spawn
 */
var registerNewCrawler = exports.registerNewCrawler = function(crawlerData){
  debug(" ~~~ Reporting registerNewCrawler: ", crawlerData.location);
  return new Promise(function(resolve, reject){

    let options = {
      method : JARVIS.registerNewCrawler.method,
      uri    : JARVIS.registerNewCrawler.endpoint,
      body   : crawlerData,
      json   : true
    };

    request(options)
      .then(parsedBody => resolve(parsedBody))
      .catch(error     => reject(error));

  }); // END promise

} // END registerNewCrawler



/**
 * Change work order status
 */
var updateWorkOrder = exports.updateWorkOrder = function(order){
  debug(" ~~~ Reporting updateWorkOrder: ",order);
  return new Promise(function(resolve, reject){

    let options = {
      method : JARVIS.updateWorkOrder.method,
      uri    : JARVIS.updateWorkOrder.endpoint,
      body   : order,
      json   : true
    };

    request(options)
      .then(parsedBody => resolve(parsedBody))
      .catch(error     => reject(error));

  }); // END promise

} // END updateWorkOrder



/**
 * reportProduct
 * Reporst all information found for a product from DCM
 */
var reportProduct = exports.reportProduct = function(product){
  debug(" ~~~ Reporting reportProduct: ",product.providerSku);
  return new Promise(function(resolve, reject){

    let options = {
      method : JARVIS.reportProduct.method,
      uri    : JARVIS.reportProduct.endpoint,
      body   : product,
      json   : true
    };

    request(options)
      .then(parsedBody => resolve(parsedBody))
      .catch(error     => reject(error));

  }); // END promise

} // END reportProduct




/**
 * deRegister
 * Reporst all information found for a product from DCM
 */
var deRegister = exports.deRegister = function(){
  debug(" ~~~ Reporting deRegister");
  return new Promise(function(resolve, reject){

    let options = {
      method : JARVIS.deRegister.method,
      uri    : JARVIS.deRegister.endpoint,
      json   : true
    };

    request(options)
      .then(parsedBody => resolve(parsedBody))
      .catch(error     => reject(error));

  }); // END promise

} // END deRegister
