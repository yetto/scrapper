
const
  debug     = require('../libs/debugConf').log('crawler:report'),
  debugErr  = require('../libs/debugConf').err('crawler:report'),
  validator = require('validator'),
  Promise   = require('bluebird'),
  request   = require('request-promise'),
  // Constants
  { RATE_LIMITS, JARVIS, _SELF } = require('../constants/constants');



/**
 * Updates crawler status
 * @status : is the scrapper busy?
 */
function updateScrapeOrder(order, status){

  // DEAL WITH THIS: ~~~ Updating Scraping Order > ID:  5972e83e4d45f268086a9bdf  Status:  Error: Evaluation timed out after 25000msec.  Are you calling done() or resolving your promises? >>> HERE <<<
  let message = (status.message ? status.message : status);
  debug(" ~~~ Updating Scraping Order > ID: ", order._id, " Status: ", message);

  let body = {
    _id : order._id,
    url : order.url,
    status : status
  } // END body

  if (!!status.message) {
    body.status = "error";
    body.error = status;
  } // END if error

  let options = {
    method : JARVIS.updateScrapeOrder.method,
    uri    : JARVIS.updateScrapeOrder.endpoint,
    body   : body,
    json   : true
  };

  return request(options);

} // END updateScrapeOrder

module.exports = {
  updateScrapeOrder : updateScrapeOrder
}