
/* ##############################
  JARVIS - SCRAPPING CONTROLLER
    ## TO RUN this app don't use "node app.js" use:
    npm start || npm run debug
############################## */

const
  debug         = require('./libs/debugConf').log('app'),
  dotenv        = require('dotenv').load(),
  express       = require('express'),
  favicon       = require('serve-favicon'),
  morgan        = require('morgan'),
  cookieParser  = require('cookie-parser'),
  bodyParser    = require('body-parser'),
  path          = require('path'),
  cors          = require('cors'),
  app           = express(),
  router        = express.Router(),
  jwt           = require('jsonwebtoken'),
  auth          = require('./routes/auth'),
  scrape        = require('./routes/scrape'),
  remote        = require('./routes/remote'),
  lander        = require('./routes/lander'),
  // Constants
  {
    MONGO_DB_CON_OPTIONS,
    PROCESS_TITLE,
    _SELF,
    SELF_URL,
    JARVIS_LOCATION,
    NODE_ENV
  } = require('./constants/constants');


process.title = PROCESS_TITLE;

// AUTH
app.set('superSecret', process.env.SECRET ||  'CREATE_A_DOT_ENV_FILE');

// ### View engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// ### App main middleware setup
app.use(morgan('tiny'));
app.use(cors());
app.options('*', cors());
app.use(favicon(path.join(__dirname, 'public/images', 'favicon.ico')));
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

/* ### Normal landing
 * root domain landing, hanlde incomming request
 */
app.use('/', lander);
// ## End-points for scrapping orders
app.use('/auth', auth);
app.use('/scrape', scrape);
//app.use('/remote', remote);



/**
 * Returns list of all routes
 */
app.get('/routes', function(req, res, next) {


  let env = process.env.NODE_ENV;
  if (env === "development" || env === "staging") listPaths();
  else next();

  function listPaths() {
    var routes  = [],
        routers = [scrape, auth, lander];
    routers.forEach(function(router) {
      router.stack.forEach(function(r) {
        if (r.route && r.route.path) {
          if (r.route.path != '/') {
            routes.push(r.route.path);
          }
        }
      })
    });
    res.json({
      routes: routes
    });
  } // END elevate

}); // END routes



// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});



// error handlers
// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    debug("\n APP > Error \n\n",err, req.path, req.method, "\n\n APP > END Error \n");
    res.json({
      message: err.message,
      error: err.status,
      req: req.path,
      method: req.method,
      body: req.body
    });
  });
}



// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});



/**
 * Prints out important info
 */
if (NODE_ENV !== "production") {
  debug(" APP > SELF_ID:         ",_SELF);
  debug(" APP > SELF_URL:        ",SELF_URL);
  debug(" APP > JARVIS_LOCATION: ",JARVIS_LOCATION);
  debug(" APP > APP ENV:         ",app.get('env'));
} // END


/* ##############################
# APP EXPORT
############################## */
app.disable('x-powered-by');
module.exports = app;