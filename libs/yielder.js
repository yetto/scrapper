const
  debug = require('debug')('yielder'),
  // Constants
  { RATE_LIMITS } = require('../constants/constants');;

/**
 * Generators helper
 */
let yielder = function(yieldFrom, data){
  setTimeout(() => {
    if (!yieldFrom) return;

    var iterable;
    var result = yieldFrom.iterator.next(data);
    //debug(yieldFrom,result);

    if(!result.done){
      iterable = result.value;
      //debug("iterable: ",typeof iterable," | Promise.then: ", typeof iterable.then);
      if (typeof iterable === "object"  && typeof iterable.then == "function") {
        //debug("Promise Yield");
        iterable
          .then(data => yielder(yieldFrom, data))
          .catch(error => yieldFrom.iterator.throw(error));

      } else {
        //debug("Normal Yield");
        yielder(yieldFrom, data);
      }

    } else if (result.done) {
      //debug("Calling done");
      yieldFrom.onDone();

    } // END if
  },RATE_LIMITS.timeThrottle);

}; // END yielder

module.exports = yielder;