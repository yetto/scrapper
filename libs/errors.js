/**
 * errors/CustomError
 */
class CustomError extends Error {
  constructor(message) {
    super(message);
    this.name = this.constructor.name;
    if (typeof Error.captureStackTrace === 'function') {

    } else {
      this.stack = (new Error(message)).stack;

    } // END if

  } // END constructor

} // END CustomError




/**
 * errors/badGateway
 */
class BadGateway extends CustomError {
  constructor(code, message){
    super(message);
    this.name       = this.constructor.name;
    this.message    = message || 'Bad Gateway';
    this.statusCode = 502;
    this.code       = code || 502;
  }

}; // END BadGateway



/**
 * errors/badRequest
 */
class BadRequest extends CustomError {
  constructor(code, message){
    super(message);
    this.name       = 'BadRequest';
    this.message    = message || 'Bad Request';
    this.statusCode = 400;
    this.code       = code || 400;
  }

}; // END BadRequest



/**
 * errors/conflict
 */
class Conflict extends CustomError {
  constructor(code, message){
    super(message);
    this.name       = this.constructor.name;
    this.message    = message || 'Conflict';
    this.statusCode = 409;
    this.code       = code || 409;
  }

}; // END Conflict



/**
 * errors/failedDependency
 * @desc  The 424 (Failed Dependency) status code means that the method could
   not be performed on the resource because the requested action
   depended on another action and that action failed.
 */
class FailedDependency extends CustomError {
  constructor(code, message){
    super(message);
    this.name       = this.constructor.name;
    this.message    = message || 'Failed Dependency';
    this.statusCode = 424;
    this.code       = code || 424;
  }

}; // END FailedDependency




/**
 * errors/forbidden
 */
class Forbidden extends CustomError {
  constructor(code, message){
    super(message);
    this.name       = this.constructor.name;
    this.message    = message || 'Forbidden';
    this.statusCode = 403;
    this.code       = code || 403;
  }

}; // END Forbidden




/**
 * errors/gatewayTimeout
 */
class GatewayTimeout extends CustomError {
  constructor(code, message){
    super(message);
    this.name       = this.constructor.name;
    this.message    = message || 'Gateway Timeout';
    this.statusCode = 504;
    this.code       = code || 504;
  }

}; // END GatewayTimeout




/**
 * errors/gone
 */
class Gone extends CustomError {
  constructor(code, message){
    super(message);
    this.name       = this.constructor.name;
    this.message    = message || 'Gone';
    this.statusCode = 410;
    this.code       = code || 410;
  }

}; // END Gone




/**
 * errors/httpVersionNotSupported
 */
class HttpVersionNotSupported extends CustomError {
  constructor(code, message){
    super(message);
    this.name       = this.constructor.name;
    this.message    = message || 'HTTP Version Not Supported';
    this.statusCode = 505;
    this.code       = code || 505;
  }

}; // END HttpVersionNotSupported




/**
 * @module errors/internalServerError
 */
class InternalServerError extends CustomError {
  constructor(code, message){
    super(message);
    this.name       = this.constructor.name;
    this.message    = message || 'Internal Server Error';
    this.statusCode = 500;
    this.code       = code || 500;
  }

}; // END InternalServerError





/**
 * errors/methodNotAllowed
 */
class MethodNotAllowed extends CustomError {
  constructor(code, message){
    super(message);
    this.name       = this.constructor.name;
    this.message    = message || 'Method Not Allowed';
    this.statusCode = 405;
    this.code       = code || 405;
  }

}; // END MethodNotAllowed




/**
 * errors/networkAuthenticationRequired
 * @desc  The client needs to authenticate to gain network access.
 * Intended for use by intercepting proxies used to control access to the network.
 */
class NetworkAuthenticationRequired extends CustomError {
  constructor(code, message){
    super(message);
    this.name       = this.constructor.name;
    this.message    = message || 'Network Authentication Required';
    this.statusCode = 511;
    this.code       = code || 511;
  }

}; // END NetworkAuthenticationRequired




/**
 * errors/notAcceptable
 */
class NotAcceptable extends CustomError {
  constructor(code, message){
    super(message);
    this.name       = this.constructor.name;
    this.message    = message || 'Not Acceptable';
    this.statusCode = 406;
    this.code       = code || 406;
  }

}; // END NotAcceptable




/**
 * errors/notFound
 */
class NotFound extends CustomError {
  constructor(code, message){
    super(message);
    this.name       = this.constructor.name;
    this.message    = message || 'The requested resource couldn\'t be found';
    this.statusCode = 404;
    this.code       = code || 404;
  }

}; // END NotFound




/**
 * errors/notImplemented
 */
class NotImplemented extends CustomError {
  constructor(code, message){
    super(message);
    this.name       = this.constructor.name;
    this.message    = message || 'Not Implemented';
    this.statusCode = 501;
    this.code       = code || 501;
  }

}; // END NotImplemented




/**
 * errors/paymentRequired
 */
class PaymentRequired extends CustomError {
  constructor(code, message){
    super(message);
    this.name       = this.constructor.name;
    this.message    = message || 'Payment Required';
    this.statusCode = 402;
    this.code       = code || 402;
  }

}; // END PaymentRequired




/**
 * errors/proxyAuthenticationRequired
 */
class ProxyAuthenticationRequired extends CustomError {
  constructor(code, message){
    super(message);
    this.name       = this.constructor.name;
    this.message    = message || 'Proxy Authentication Required';
    this.statusCode = 407;
    this.code       = code || 407;
  }

}; // END ProxyAuthenticationRequired




/**
 * errors/requestTimeout
 */
class RequestTimeout extends CustomError {
  constructor(code, message){
    super(message);
    this.name       = this.constructor.name;
    this.message    = message || 'Request Timeout';
    this.statusCode = 408;
    this.code       = code || 408;
  }

}; // END RequestTimeout




/**
 * errors/serviceUnavailable
 */
class ServiceUnavailable extends CustomError {
  constructor(code, message){
    super(message);
    this.name       = this.constructor.name;
    this.message    = message || 'Service Unavailable';
    this.statusCode = 503;
    this.code       = code || 503;
  }

}; // END ServiceUnavailable



/**
 * errors/unauthorizedRequest
 */
class Unauthorized extends CustomError {
  constructor(code, message){
    super(message);
    this.name       = this.constructor.name;
    this.message    = message || 'Unauthorized Request';
    this.statusCode = 401;
    this.code       = code || 401;
  }

}; // END Unauthorized




/**
 * errors/unprocessableEntity
 * @desc The 422 (Unprocessable Entity) status code means the server understands the content type of the request entity,
 * and the syntax of the request entity is correct but was unable to process the contained instructions.
 */
class UnprocessableEntity extends CustomError {
  constructor(code, message){
    super(message);
    this.name       = this.constructor.name;
    this.message    = message || 'Unprocessable Entity';
    this.statusCode = 422;
    this.code       = code || 422;
  }

}; // END UnprocessableEntity



module.exports = {
  BadGateway                    : BadGateway,
  BadRequest                    : BadRequest,
  Conflict                      : Conflict,
  FailedDependency              : FailedDependency,
  Forbidden                     : Forbidden,
  GatewayTimeout                : GatewayTimeout,
  Gone                          : Gone,
  HttpVersionNotSupported       : HttpVersionNotSupported,
  InternalServerError           : InternalServerError,
  MethodNotAllowed              : MethodNotAllowed,
  NetworkAuthenticationRequired : NetworkAuthenticationRequired,
  NotAcceptable                 : NotAcceptable,
  NotFound                      : NotFound,
  NotImplemented                : NotImplemented,
  ProxyAuthenticationRequired   : ProxyAuthenticationRequired,
  PaymentRequired               : PaymentRequired,
  RequestTimeout                : RequestTimeout,
  ServiceUnavailable            : ServiceUnavailable,
  Unauthorized                  : Unauthorized,
  UnprocessableEntity           : UnprocessableEntity

} // END exports