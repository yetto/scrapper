
const
  dotenv              = require('dotenv').load(),
  jwt                 = require('jsonwebtoken'),
  { PERMISSION_SETS,
    SECRET,
    ADMIN_SECRET }    = require('../constants/constants'),
  { AUTH }            = require('../constants/messages');

let token;

function decodeToken(req, res, callback) {

  debug("token", token);

  // 1.- Attempt to verify normal user
  jwt.verify(token, SECRET, verifyUser);

  function verifyUser(err, decoded){
    // 2.- if normal user error, check if it is admin
    if (err) jwt.verify(token, ADMIN_SECRET, verifyAdmin);

    if(decoded) {
      debug("User token decoded: ", decoded);
      res.locals.userID = decoded.userID;
      callback(decoded.perms);

    } // END if

  } // END verifyUser

  function verifyAdmin(err, decoded){
    debug("Admin token decoded: ", decoded);
    if(decoded) {
      res.locals.userID = decoded.userID;
      callback(decoded.perms);
      return;

    } // END if
    callback(null);

  } // END verifyAdmin

}; // END decodeToken



function evaluatePerms(userPerm, pathPerms) {
  if (userPerm === null) return false;

  userPerm = (typeof userPerm === 'string')
                ? [userPerm]
                : userPerm;

  pathPerms = (typeof pathPerms === 'string')
                ? [pathPerms]
                : pathPerms;

  let res = userPerm.map(function(perm, index, array) {
    for (var i = 0; i < pathPerms.length; i++) {
      if (perm === pathPerms[i]) return true;
    }
  });

  if (res.indexOf(true) != -1) return true
  else return false;

}; // evaluatePerms



/* #####
# MODULE exports: PERMS
##### */
let debug;
let Perms = function(optional,verbose) {
  // Expose debug
  if   (verbose) debug = optional || function(){};
  else debug = function(){};

  // Proto properties
  this.badPerms = AUTH.error.denied;
  this.sets     = PERMISSION_SETS;

}; // END Perms



Perms.prototype.check = function(checkFor) {
  return _middleware.bind(this);

  function _middleware(req, res, next) {

    debug("checkFor",checkFor);

    // world: open perms
    if (checkFor[0] === "world") {
      debug('Perms.verify: ', checkFor);
      next();
      return;
    }

    token = req.headers['x-access-token']
      || res.locals.token
      || req.body.token
      || req.query.token;

    // decode and validate against route perms
    decodeToken(req, res, (decodedPerms) => {
      debug('Perms.verify: ', decodedPerms, " > ", checkFor);
      if (evaluatePerms(decodedPerms, checkFor)) next();
      else res.status(401).json(this.badPerms);

    });

  } // END _middleware

}; // END check



module.exports = function(optional,verbose) {
  //console.log(optional,verbose);
  //verbose = true;
  return new Perms(optional,verbose);
}; // END exports