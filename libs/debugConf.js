const
  debug = require('debug');

exports.log = (nameSpace) => {
  if (typeof nameSpace !== "string") throw "Debug Conf: nameSpace must be a string"
  var stdOut = debug("log:" + nameSpace.toString());
  stdOut.log = console.log.bind(console); // don't forget to bind to console!

  return function() {
    Array.prototype.unshift.apply(arguments, [process.pid]);
    return stdOut(...arguments);
  } // END return

} // END log


exports.err = (nameSpace) => {
  if (typeof nameSpace !== "string") throw "Debug Conf: nameSpace must be a string"
  var stdErr = debug("err:" + nameSpace.toString());
  return function() {
    Array.prototype.unshift.apply(arguments, [process.pid]);
    return stdErr(...arguments);
  } // END return

} // END err
