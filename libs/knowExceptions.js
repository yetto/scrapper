
const
  _        = require('lodash/core'),
  { HTTP } = require('../constants/messages');

let debug;

function _handleErrObj(req, res, err, context) {

  if (!!debug) debug(context, err.message);
  if (!!debug) debug(err);

  // Mongo Unique field exist
  if (err.message.indexOf('E11000') != -1) {
    res.status(HTTP.c400.code).json({
      message :  HTTP.c400.message,
      code : HTTP.c400.code,
      cue : "A provided value was expected to be unique and already exists",
      context : context
    });
    return false;
  }

  // ?
  if (err.message.indexOf('validation failed') != -1){

    res.status(HTTP.c400.code).json({
      message :  HTTP.c400.message,
      code : HTTP.c400.code,
      cue : 'Double check provided information. A validation error ocurred.',
      context : context
    });
    return false;
  }

  res.status(HTTP.c500.code).json({
    message : HTTP.c500.message,
    context : context
  });

} // EMD _handleErrObj

function _handleErrStr(req, res, err, context) {

  if (!!debug) debug(context);
  if (!!debug) debug(err);

  res.status(HTTP.c500.code).json({
    message : HTTP.c500.message,
    error : err,
    context : context
  });
}

module.exports = function(req, res, err, context, debugObj) {
  debug = debugObj;
  if (typeof err === 'object') _handleErrObj(req, res, err, context);
  if (typeof err === 'string') _handleErrStr(req, res, err, context);
};