
const
  debug    = require('../libs/debugConf').log('answer'),
  debugErr = require('../libs/debugConf').err('answer'),
  { HTTP } = require('../constants/messages'),
  // Constants
  {
    NODE_ENV
  } = require('../constants/constants');



/**
 * composeError
 */
let composeError = exports.composeError = (response, errorInstance, verbose) => {
  if (verbose) debug(errorInstance);
  let body = {
    code    : errorInstance.code,
    message : errorInstance.message
  } // END body

  if(NODE_ENV === "development") body["dev"] = {
    stack : errorInstance.stack,
    message : errorInstance.message
  };

  response
    .status(errorInstance.statusCode)
    .json(body); // END response

} // END composeError




/**
 * composeResponse
 */
let composeResponse = exports.composeResponse = (response, responseInstance, verbose) => {
  if (verbose) debug(responseInstance);
  let body = {
    code    : responseInstance.code,
    message : responseInstance.message
  } // END body

  if(responseInstance.data)      body["data"] = responseInstance.data;
  if(NODE_ENV === "development") body["code"] = responseInstance.code;

  response
    .status(responseInstance.statusCode)
    .json(body); // END response

} // END composeResponse




/**
 * composeReport
 */
let composeReport = exports.composeReport = (reportInstance, verbose) => {
  if (verbose) debug(reportInstance);
  let body = {
    code    : reportInstance.code,
    message : reportInstance.message
  } // END body

  if(reportInstance.data) body["data"] = reportInstance.data;

  return reportInstance.statusCode;

} // END composeReport



/**
 * parseError
 */
let parseError = exports.parseError = (prefix, error, verbose) => {
  if (verbose) debugErr({ internalErrorCode: prefix, error : error });

  // Proxy could not be reached
  if (error.message.indexOf('tunneling socket could not be established') != -1) {
    return {
      code : prefix + "e03",
      message : 'Upstream proxy could not be reached'
    };

  } // END if


  // Nighmare waited long enough
  if (error.message.indexOf('.wait() timed out after') != -1) {
    return {
      code : prefix + "e04",
      message : 'One or more values where expected to be unique.'

    };

  } // END if


  // Unknow
  debugErr({ internalErrorCode: prefix, error : error });
  return {
    code : prefix + "e00",
    message : "Unknow error"

  };

} // EMD _composeErrObj
