
/** ##########################
 * Project utils
 ########################## */

/**
 * Parse URL:  ":param" to  "/param/"
 * PARAMS:
 *  @url       {string}   traget url
 *  @urlParams {object}   url params to replace
 *  @return    {string}   parsed url
 */
var parseUrl = exports.parseUrl = (url, urlParams) => {
  if (url.indexOf(":") === -1) return url;

  Object.keys( urlParams ).forEach(function( key ){
    if( url.indexOf(key) !== -1){
      url = url.replace(":"+key,encodeURI(urlParams[key]));
    } // END if
  }); // END ObjKeys

  return url;

} // END parseUrl
