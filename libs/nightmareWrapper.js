
const
  Nightmare          = require('nightmare'),
  Promise            = require("bluebird"),
  // Constants
  { NODE_ENV,
    RATE_LIMITS,
    PROXIES,
    BROWSER_WINDOW } = require('../constants/constants');


/** ########################################################
 *  # DCM Nightmare:
 ######################################################## */

// Nightmare options
nightmareOptions = {}

if      (NODE_ENV === "development") nightmareOptions = { show: BROWSER_WINDOW, width: 1024, height: 768 }
else if (NODE_ENV === "staging")     nightmareOptions = { show: false }
else if (NODE_ENV === "production")  nightmareOptions = { show: false }

let nightmareTimeouts = {
  waitTimeout      : RATE_LIMITS.nmTimeouts.waitTimeout,
  gotoTimeout      : RATE_LIMITS.nmTimeouts.gotoTimeout,
  loadTimeout      : RATE_LIMITS.nmTimeouts.loadTimeout,
  executionTimeout : RATE_LIMITS.nmTimeouts.executionTimeout
} // END nightmareTimeouts

let proxy = {
  switches: {
    'proxy-server': PROXIES.main[0],
    'ignore-certificate-errors': true
  }
}

Object.assign(nightmareOptions, nightmareTimeouts);

// Nightmare prototype extension and instantiation for DCM
exports.hookActions = methods      => Nightmare.action(methods.nameSpace, methods.actionsObject);
exports.start       = extraOptions => Nightmare(Object.assign(nightmareOptions, extraOptions));
exports.kill        = instance     => instance.end();