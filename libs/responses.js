/**
 * errors/CustomResponse
 */
class CustomResponse {
  constructor(code, message, data) {
    this.name = this.constructor.name;

  } // END constructor

} // END CustomResponse



/**
 * errors/badGateway
 */
class Ok extends CustomResponse {
  constructor(code, message, data){
    super(code, message, data)
    this.data       = data || null;
    this.name       = this.constructor.name;
    this.message    = message || "OK";
    this.statusCode = 200;
    this.code       = code || 200;

  } // END constructor

}; // END Ok



/**
 * errors/badGateway
 */
class MultipleChoices extends CustomResponse {
  constructor(code, message, data){
    super(code, message, data)
    this.data       = data || null;
    this.name       = this.constructor.name;
    this.message    = message || "Moved Permanently";
    this.statusCode = 300;
    this.code       = code || 300;

  } // END constructor

}; // END MultipleChoices



/**
 * errors/badGateway
 */
class MovedPermanently extends CustomResponse {
  constructor(code, message, data){
    super(code, message, data)
    this.data       = data || null;
    this.name       = this.constructor.name;
    this.message    = message || "Found";
    this.statusCode = 302;
    this.code       = code || 302;

  } // END constructor

}; // END MovedPermanently


/**
 * errors/badGateway
 */
class Found extends CustomResponse {
  constructor(code, message, data){
    super(code, message, data)
    this.data       = data || null;
    this.name       = this.constructor.name;
    this.message    = message || "Not Modified";
    this.statusCode = 304;
    this.code       = code || 304;

  } // END constructor

}; // END Found



/**
 * errors/badGateway
 */
class NotModified extends CustomResponse {
  constructor(code, message, data){
    super(code, message, data)
    this.data       = data || null;
    this.name       = this.constructor.name;
    this.message    = message || "Temporary Redirect";
    this.statusCode = 307;
    this.code       = code || 307;

  } // END constructor

}; // END NotModified


module.exports = {
  Ok               : Ok,
  MultipleChoices  : MultipleChoices,
  MovedPermanently : MovedPermanently,
  Found            : Found,
  NotModified      : NotModified

} // END module.exports