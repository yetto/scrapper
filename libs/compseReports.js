/**
 * reports/CustomReport
 */
class CustomReport {
  constructor(code, message, data) {
    this.name = this.constructor.name;

  } // END constructor

} // END CustomReport



/**
 * reports/Success
 */
class Success extends CustomReport {
  constructor(code, message, data){
    super(code, message, data)
    this.data       = data || null;
    this.name       = this.constructor.name;
    this.message    = message || "OK";
    this.code       = code || code + "e1";

  } // END constructor

}; // END Success



/**
 * reports/Failure
 */
class Failure extends CustomReport {
  constructor(code, message, data){
    super(code, message, data)
    this.data       = data || null;
    this.name       = this.constructor.name;
    this.message    = message || "Something went wrong";
    this.code       = code || code + "e2";

  } // END constructor

}; // END Failure



/**
 * reports/TryAgain
 */
class TryAgain extends CustomReport {
  constructor(code, message, data){
    super(code, message, data)
    this.data       = data || null;
    this.name       = this.constructor.name;
    this.message    = message || "Try again later";
    this.code       = code || code + "e3";

  } // END constructor

}; // END TryAgain


/**
 * reports/NotFound
 */
class NotFound extends CustomReport {
  constructor(code, message, data){
    super(code, message, data)
    this.data       = data || null;
    this.name       = this.constructor.name;
    this.message    = message || "Not found";
    this.code       = code || code + "e4";

  } // END constructor

}; // END NotFound



/**
 * reports/TemporarilyOffline
 */
class TemporarilyOffline extends CustomReport {
  constructor(code, message, data){
    super(code, message, data)
    this.data       = data || null;
    this.name       = this.constructor.name;
    this.message    = message || "Temporarily Offline";
    this.code       = code || code + "e5";

  } // END constructor

}; // END TemporarilyOffline



/**
 * reports/InvalidData
 */
class InvalidData extends CustomReport {
  constructor(code, message, data){
    super(code, message, data)
    this.data       = data || null;
    this.name       = this.constructor.name;
    this.message    = message || "Invalid Data";
    this.code       = code || code + "e6";

  } // END constructor

}; // END InvalidData



module.exports = {
  Success            : Success,
  Failure            : Failure,
  TryAgain           : TryAgain,
  NotFound           : NotFound,
  TemporarilyOffline : TemporarilyOffline,
  InvalidData        : InvalidData

} // END module.exports