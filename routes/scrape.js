
const
  debug                 = require('../libs/debugConf').log('scrape:route'),
  debugErr              = require('../libs/debugConf').err('scrape:route'),
  express               = require('express'),
  router                = express.Router(),
  perms                 = require('../libs/jwtRoles')(), // (debug),
  knowException         = require('../libs/knowExceptions'),
  // Controllers
  scrapeController      = require('../controllers/scrapeController'),
  { updateCrawler }     = require('../reports/crawler'),
  { updateScrapeOrder } = require('../reports/scrapeOrders'),
  request               = require('request-promise'),
  // Constants
  { SECRET,
    ADMIN_SECRET,
    NODE_ENV,
    PROXIES,
    _SELF,
    SELF_URL } = require('../constants/constants'),
  // Replies
  {
    BadGateway,
    BadRequest,
    Conflict,
    FailedDependency,
    Forbidden,
    GatewayTimeout,
    Gone,
    HttpVersionNotSupported,
    InternalServerError,
    MethodNotAllowed,
    NetworkAuthenticationRequired,
    NotAcceptable,
    NotFound,
    NotImplemented,
    PaymentRequired,
    ProxyAuthenticationRequired,
    RequestTimeout,
    ServiceUnavailable,
    Unauthorized,
    UnprocessableEntity
  } = require('../libs/errors'),
  {
    Ok,
    MultipleChoices,
    MovedPermanently,
    Found,
    NotModified
  } = require('../libs/responses'),
  answers = require('../libs/answers');



/** ########################################################
 *  # Scrapping: Normal Routes

    # WORK ORDER
      url              : String,
      template         : String,
      templateData     : Schema.Types.Mixed,
      connection       : Schema.Types.Mixed,
      scrapperId       : String,
      scrapperLocation : String

 ######################################################## */


let messages = {
  busy : "Scrapper currently busy, try again later",
  accepted : "Order accepted, scrapper is now busy",
  noStatus : "Please send a valid status, ie: { busy: false }"

} // END messages

let currentOrder = {
  _id              : null,
  url              : null,
  template         : null,
  templateData     : null,
  connection       : null,
  error            : null,
  scrapperId       : null,
  scrapperLocation : null,
  // 'new', 'error', 'done', 'active'
  status           : null

}; // END currentOrder

let defaultOrder = {};
Object.assign(defaultOrder, currentOrder);

let crawler = {
  _self     : _SELF,
  spawnDate : Date.now(),
  location  : SELF_URL,
  busy      : false

}; // END crawler


/*
 * GET scrape/status
 * scrapper status
 */
router.get('/status', perms.check(perms.sets.world), function(req, res) {
  let status = {
    crawler: crawler,
    currentOrder: currentOrder
  } // END status

  debug(' | scrape/status > Location: ', status.crawler.location, " Busy: ", status.crawler.busy);
  answers
    .composeResponse(res, new Ok("scst3", "Status report", status));

}); // END GET scrape/




/*
 * GET scrape/status/notbusy
 * scrapper status
 */
router.get('/status/notbusy', perms.check(perms.sets.world), function(req, res) {

  crawler.busy = false;
  updateCrawler(false);
  debug(' | status/notbusy > Status: ', crawler.busy);
  answers
    .composeResponse(res, new Ok("scnb1", "Status set to not busy", crawler));

}); // END GET scrape/




/*
 * POST scrape/status/notbusy
 * scrapper status
 */
router.post('/status', perms.check(perms.sets.world), function(req, res) {

  if (typeof req.body.busy !== "boolean") {
    // Response
    answers
      .composeError(res, new BadRequest("scst0", messages.noStatus));

  } else {
    let { busy } = req.body;
    crawler.busy = busy;
    debug(' | status > Crawler.busy: ', crawler.busy);
    answers
      .composeResponse(res, new Ok("scst1", "Status has been set", crawler));

  } // END if

}); // END GET scrape/



/** ########################################################
 *  # Scrapping: Order Routes
 ######################################################## */



/*
 * POST scrape/order
 * creates a new order petition
 */
router.post('/order', perms.check(perms.sets.world), function(req, res) {
  debug(" | scrape/order > Scrapper busy? ", crawler.busy);

  if (crawler.busy) {
    answers
      .composeResponse(res, new Ok("scor2", messages.busy, {
        order : currentOrder,
        busy : crawler.busy

      }));

  } else {

    Object.assign(currentOrder, defaultOrder);
    Object.keys(req.body).forEach(key => currentOrder[key] = req.body[key]);
    debug(" | scrape/order > Current order: ", currentOrder.template);

    updateCrawler(true);
    scrapeController
      .parseOrder(currentOrder)
      .then(data => {
        // Report succes
        updateScrapeOrder(currentOrder, "done")
          .then(response => {
            debug(' | scrape/order > Order: ', response.data._id, " Status: ", response.data.status)
            return updateCrawler(false);

          })
          .catch(error => debugErr(error));


      })
      .catch(error => {
        debugErr(' | scrape/order > error: ', error.message);
        updateScrapeOrder(currentOrder, error)
          .then(result => updateCrawler(false))
          .catch(error => debugErr(error));

      }); // END scrapeController

      // Response
      answers
        .composeResponse(res, new Ok("scor1", messages.accepted, {
          order : currentOrder,
          busy : crawler.busy

        }));

  } // END if

}); // END router.post('/'




module.exports = router;