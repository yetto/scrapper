
const
  debug          = require('../libs/debugConf').log('auth:route'),
  debugErr       = require('../libs/debugConf').err('auth:route'),
  express        = require('express'),
  jwt            = require('jsonwebtoken'),
  validator      = require('validator'),
  router         = express.Router(),
  perms          = require('../libs/jwtRoles')(debug),
  request        = require('request-promise'),
  // Constants
  { SECRET,
    ADMIN_SECRET,
    NODE_ENV,
    PROXIES } = require('../constants/constants'),
  // Replies
  {
    BadGateway,
    BadRequest,
    Conflict,
    FailedDependency,
    Forbidden,
    GatewayTimeout,
    Gone,
    HttpVersionNotSupported,
    InternalServerError,
    MethodNotAllowed,
    NetworkAuthenticationRequired,
    NotAcceptable,
    NotFound,
    NotImplemented,
    PaymentRequired,
    ProxyAuthenticationRequired,
    RequestTimeout,
    ServiceUnavailable,
    Unauthorized,
    UnprocessableEntity
  } = require('../libs/errors'),
  {
    Ok,
    MultipleChoices,
    MovedPermanently,
    Found,
    NotModified
  } = require('../libs/responses'),
  answers = require('../libs/answers');

/*
 * GET /app/expire
 * Expire JWT (logout)
 */
router.get('/verify', function(req, res) {
  debug('get - app/verify');
  var token = req.body.token || req.query.token || req.headers['x-access-token'];

  jwt.verify(token, SECRET, function(err, decoded) {
    if (decoded) success(decoded.perms);
    if (decoded) return true;
    jwt.verify(token, ADMIN_SECRET, function(err, decoded) {
      if (err) failed();
      else success(decoded.perms);

    });

  });

  function success() {
    // Response
    answers
      .composeResponse(res, new Ok("auve1", 'Token is valid', { status : "Ok" }));

  } // END success

  function failed() {
    // Response
    answers
      .composeError(res, new Unauthorized("auve0", 'Token has expired or is not valid'));

  } // END failed

});


/*
 * GET /app/admin
 * Expire JWT (logout)
 */
router.get('/admin', perms.check(perms.sets.urw), function(req, res) {

  var userID = req.params.userID ? req.params.userID : res.locals.userID;
  debug('adminClaim', userID);

  if (NODE_ENV === "development") elevate();
  else notFound();

  function notFound() {
    return res.status(404).json({
      message: 'Not Found',
      error: err

    });

  } // notFound

  function elevate() {
    userModel.findOne({
      _id: userID

    }, function(err, user) {
      if (err) notFound();
      if (!user) notFound();
      user.perms = perms.sets.all;
      user.save(function(err, user) {
        if (err) notFound();
        if (typeof callback === 'function') callback(user);
        else return res.json(user);

      }); // end user.save

    });

  } // END elevate

});



/*
 * GET /app/expire
 * Expire JWT (logout)
 */
router.get('/expire', function(req, res, next) {
  /* :TODO:
    Write file to disk, read list, keep in memory, reload list from disk on write or del
    Compare list on req
  */
  res.status(426).json({
    res: "Upgrade Required"

  });

});


/*
 * GET /app/proxy-test
 * scrapper status
 */
router.get('/proxy-test', perms.check(perms.sets.world), function(req, res) {

  debug('/app/proxy-test');

  let options = {
    proxy: PROXIES.main[0],
    method: "GET",
    uri: "https://api.ipify.org/?format=json"
  };

  request(options)
    .then(ipInfo => {
      debug(' +++ PROXY TEST: ', ipInfo);

      // Response
      answers
        .composeResponse(res, new Ok("aupr1", "Proxy OK", ipInfo));

    })
    .catch(error => {
      let { code, message } = answers.parseError("aupr0", error);
      if (!message) message = 'Error when trying to reach proxy.';

      // Response
      answers
        .composeError(res, new GatewayTimeout(code, message));

    });

}); // END GET scrape/


/*
 * GET /app/proxy-test
 * scrapper status
 */
router.get('/pid', function(req, res) {

  debug('/app/pid');

  // Response
  answers
    .composeResponse(res, new Ok("aupi1", "Proxy OK", { pid : process.pid }));

}); // END GET scrape/


module.exports = router;