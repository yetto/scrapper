const
  debug              = require('../libs/debugConf').log('scrape:route'),
  debugErr           = require('../libs/debugConf').err('scrape:route'),
  express            = require('express'),
  router             = express.Router(),
  {
    BadGateway,
    BadRequest,
    Conflict,
    FailedDependency,
    Forbidden,
    GatewayTimeout,
    Gone,
    HttpVersionNotSupported,
    InternalServerError,
    MethodNotAllowed,
    NetworkAuthenticationRequired,
    NotAcceptable,
    NotFound,
    NotImplemented,
    PaymentRequired,
    ProxyAuthenticationRequired,
    RequestTimeout,
    ServiceUnavailable,
    Unauthorized,
    UnprocessableEntity
  }                 = require('../libs/errors'),
  {
    Ok,
    MultipleChoices,
    MovedPermanently,
    Found,
    NotModified
  }                 = require('../libs/responses'),
  answers           = require('../libs/answers'),
  // Constants
  { SELF_LOCATION,
    NODE_ENV,
    SELF_URL,
    _SELF,
    PROCESS_TITLE,
    RATE_LIMITS,
    BROWSER_WINDOW } = require('../constants/constants');


/*
 * GET /
 * list all scrape
 */
router.get('/', function(req, res) {

  if (NODE_ENV === "development" || NODE_ENV === "staging") {

    answers
      .composeResponse(res, new Ok("lain1", 'Server info', {
        SELF_LOCATION,
        NODE_ENV,
        SELF_URL,
        _SELF,
        PROCESS_TITLE,
        RATE_LIMITS,
        BROWSER_WINDOW

      })); // END answers.composeResponse

  } else {

    answers
      .composeResponse(res, new MovedPermanently()); // END answers.composeResponse

  } // END if

});

module.exports = router;