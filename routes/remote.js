const
  debug     = require('../libs/debugConf').log('scrape:route'),
  debugErr  = require('../libs/debugConf').err('scrape:route'),
  express           = require('express'),
  router            = express.Router(),
  perms             = require('../libs/jwtRoles')(debug),
  knowException     = require('../libs/knowExceptions'),
  // Controllers
  scrapeController  = require('../controllers/scrapeController');



/*
 * GET remote/
 * scrapper status
 */
router.get('/', function(req, res) {

  debug('remote/');
  scrapeController
    .newJob()
    .then(response => {
      debug('POST:201 - remote/',response);
      res.status(201).json(response);

    })
    .catch(error => {
      let exception = knowException(error, 'Error when creating scrapping order.');
      debug('POST - remote/',exception,error);
      res.status(exception.code).json({
          message: exception.message,
          error: error
      });

    }); // END scrapeController

}); // END GET remote/



/*
 * POST remote/
 * create a scrape decoupled from a user
 */
router.post('/', perms.check(perms.sets.aw), function(req, res) {

  let { users, run, connection } = req.body;
  scrapeController
    .newJob({ users, run, connections })
    .then((response) => {
      let { busy, spawnDate, location } = response;
      debug('POST:201 - remote/',{ busy, spawnDate, location });
      res.status(201).json({ busy, spawnDate, location });
    })
    .catch((error) => {
      let exception = knowException(err, 'Error when creating scrapping order.');
      debug('POST - remote/',exception,err);
      res.status(exception.code).json({
          message: exception.message
      });
    });

}); // END router.post('/'



/*
 * PUT remote/:spawnDate
 * Appends new task to queue
 */
router.put('/:spawnDate', perms.check(perms.sets.aw), function(req, res) {

  let { users, run, connection } = req.body,
      { spawnDate }              = req.params;

  scrapeController
    .queueJob({ users, run, connections })
    .then((response) => {
      let { busy, spawnDate, location } = response;
      debug('PUT:201 - remote/:spawnDate',{ busy, spawnDate, location });
      res.status(201).json({ busy, spawnDate, location });
    })
    .catch((error) => {
      let exception = knowException(err, 'Error when creating scrapping order.');
      debug('PUT - remote/:spawnDate',exception,err);
      res.status(exception.code).json({
          message: exception.message
      });
    });

}); // END router.put('/:spawnDate'



module.exports = router;