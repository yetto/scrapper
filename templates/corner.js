
const
  debug          = require('../libs/debugConf').log('corner:template'),
  debugErr       = require('../libs/debugConf').err('corner:template'),
  RateLimiter    = require('limiter').RateLimiter,
  validator      = require('validator'),
  Promise        = require("bluebird"),
  co             = require("co"),
  request        = require('request-promise'),
  // Report
  { notBusy }    = require('../reports/crawler'),
  reportProduct  = require("../reports/product"),
  bulkReports    = require("../reports/bulk"),
  // Constants
  { RATE_LIMITS,
    NODE_ENV,
    PROXIES }    = require('../constants/constants'),
  limiter        = new RateLimiter(RATE_LIMITS.howMany, RATE_LIMITS.every);


// Corner template data
let entryPoint  = "https://cornershopapp.com/api/v1/branches/:store",
    productsUrl = "https://cornershopapp.com/api/v1/branches/:store/aisles/:subcategory/products",
    stores = { wallmart : { id : 59 }, costco   : { id : 92 }, chedraui : { id : 138 } };


var corner_discovery = exports.corner_discovery = () => {
  debug(" === corner_discovery");
  return new Promise(function(resolve, reject){

    let = sotresArr = [];
    Object.keys(stores).forEach((name, index) => {
      let options = {
        method: "GET",
        uri: parse_url(entryPoint, { target: ":store", value: stores[name].id })

      } // END options

      if (NODE_ENV !== "development") options.proxy = PROXIES.main[0];

      sotresArr.push({ options : options, storeName : name });

    });

    Promise
      .all(sotresArr.map(store => {
        debug(" === corner_discovery > URL: ",store.options.url, " PROXY: ", store.options.proxy);
        return request(store.options)
          .then(parsedBody => parse_store(JSON.parse(parsedBody), store.storeName))
          .then(storeObj => discoveryOrders(storeObj))
          .then(orders => bulkReports.orders(orders))

      }))
      .then(result => {
        debug(" === corner_discovery > DONE",result);
        resolve(result);
      })
      .catch(reject);

  }); // END promise

} // END reportProduct


var corner_aisle = exports.corner_aisle = (aisle) => {
  debug(" === corner_aisle");
  return new Promise(function(resolve, reject){

    parse_aisle(aisle)
      .then(products => bulkReports.products(products))
      .then(result => resolve(result))
      .catch(error => debug(error));

  }); // END promise

} // END corner_aisle


var corner_discovery = exports.corner_single = (corner_id) => {
  debug(" === corner_discovery");
  return new Promise(function(resolve, reject){





  }); // END promise

} // END reportProduct




/** ########################################################
 *  # HELPERS
 ######################################################## */


function parse_url(url,params){
  let retVal = url;
  if (Array.isArray(params)) {
    params.forEach(param => {
      if (typeof param === "object") retVal = retVal.replace(param.target,param.value);

    }); // END forEach

    return retVal;

  } // END if

  if (typeof params === "object") retVal = retVal.replace(params.target,params.value);
  return retVal;

} // END parse_url



function parse_store(storeObj,storeName){
  //debug("parse_store",storeObj,storeName);
  return new Promise(function(resolve, reject){
    if (typeof storeObj !== "object") return;

    stores[storeName].categories = {};
    storeObj.departments.forEach(department => {

      stores[storeName].name = "corner_" + storeName;

      stores[storeName].categories[department.name] = {
        id: department.id,
        subcategories: {}
      };

      department.aisles.forEach(aisle => {
        stores[storeName].categories[department.name].subcategories[aisle.name] = aisle.id;

      });

    });

    debug(" === parse_store: ", storeName, " id: ", stores[storeName].id);
    resolve({ storeName : storeName, data : stores[storeName], id: stores[storeName].id });

  }); // END promise

} // END parse store



function parse_aisle(aisle){
  debug(" === parse_aisle");
  return new Promise(function(resolve, reject){

    let { url, firstOrderId, category, subCategory, storeName } = aisle,
          options = { method: "GET", uri: url },
          parsedProducts = [];

    if (NODE_ENV !== "development") options.proxy = PROXIES.main[0];

    debug(" === parse_aisle > URL: ", options.url, " PROXY: ", options.proxy);
    request(options)
      .then(response => {
        let products = JSON.parse(response);

        products.forEach((product, index) => {
          parsedProducts.push({
            name : product.name,
            description : product.description,
            price : product.original_branch_price,
            brand : ( product.brand ? product.brand.name : null),
            provider : storeName,
            providerSku : firstOrderId +"-"+ product.id,
            globalSku : null,
            partNumber : null,
            images : [{ url: product.img_url }],
            attributes : null,
            currency : product.currency,
            category : category.name,
            subCategory : subCategory.name,
            stock : null,
            _scrapeDate : Date.now(),
            _lastStatus : response.status,
            _sourceUrl : url

          }); // END info

        }); // END products.forEach

        resolve(parsedProducts);

      })
      .catch(error => {
        resolve();
        debug(error);
      }); // END req

  }); // END promise

} // END parse product



function discoveryOrders(storeObj){
  return new Promise(function(resolve, reject){
    workOrders = [];
    Object
      .keys(storeObj.data.categories)
      .forEach(categoryName => {

        Object
          .keys(storeObj.data.categories[categoryName].subcategories)
          .forEach(subcategoriesName => {

            let catId = storeObj.data.categories[categoryName].id,
                subId = storeObj.data.categories[categoryName].subcategories[subcategoriesName];

            params = [{
              target: ":store",
              value: storeObj.data.id

            },{
              target: ":subcategory",
              value: storeObj.data.categories[categoryName].subcategories[subcategoriesName]

            }];

            workOrders.push({
              url: parse_url(productsUrl,params),
              template: "corner_aisle",
              templateData : {
                url: parse_url(productsUrl,params),
                category     : categoryName,
                subCategory  : subcategoriesName,
                firstOrderId : storeObj.id +"-"+ catId +"-"+ subId,
                storeName    : storeObj.storeName
              }

            });

          });

      });

    resolve(workOrders);

  }); // END promise

} // END parse store
