const
  debug              = require('../libs/debugConf').log('dcm:helpers'),
  debugErr           = require('../libs/debugConf').err('dcm:helpers'),
  // Constants
  { RATE_LIMITS } = require('../constants/constants');

/**
 * Login to a DCM account.
 * @param {Object} loginOptions: login options
 */
var login = exports.login = function(loginOptions) {
  debug(" === Login");
  return function(nightmare) {
    nightmare
      .goto("http://dcm.com.mx/login.asp")
      // Login > wait for
      .wait('#Layer1 input[name="Usuario"]')
      // Actions
      .type('#Layer1 input[name="Usuario"]',    loginOptions.user)
      .type('#Layer1 input[name="Contrasena"]', loginOptions.pass)
      .type('#Layer1 input[name="Clave"]',      loginOptions.key)
      .wait(700)
      .click('input[type=image][src="/dibujos/botones/entrar_dcm.gif"]')
      .wait("#new_tipodecambio")
      .evaluate(function(){ return true; });

  }; // END return

}; // END login


/**
 * Search for a product
 * @param {String} search
 */
var search = exports.search = function(searchTerm) {
  debug(" === Search");
  return function(nightmare) {
    nightmare
      // Search > wait for
      .type('#buscar > input.barradebusqueda',  searchTerm)
      .wait(700)
      .click('#botonbuscar a:first-child')
      .wait('#ordenar_busqueda')
      .evaluate(function(){ return true; });

  }; // END return

}; // END search

