
const
  debug             = require('../libs/debugConf').log('portal:template'),
  debugErr          = require('../libs/debugConf').err('portal:template'),
  Nightmare         = require('nightmare'),
  RateLimiter       = require('limiter').RateLimiter,
  validator         = require('validator'),
  jquery            = require('jquery'),
  Promise           = require("bluebird"),
  co                = require("co"),
  yielder           = require("../libs/yielder"),
  dotenv            = require('dotenv').load(),
  env               = process.env,
  { notBusy }       = require('../reports/crawler'),
  // Constants
  { RATE_LIMITS }   = require('../constants/constants'),
  limiter           = new RateLimiter(RATE_LIMITS.howMany, RATE_LIMITS.every);



/**
 * Main Stats
 */
let crawlerUrls = {
  categories    : [],
  subCategories : [],
  pages         : [],
  products      : []
}; // END crawlerUrls



/**
 * Messages
 */
let errors = {
  login : "Something went wrong when loogin into DCM"
}; // END errors



let tick = setTimeout(endNightmare,RATE_LIMITS.tick);

function renewTick(){
  clearTimeout(tick);
  tick = setTimeout(endNightmare,RATE_LIMITS.tick);
} // END renewTick

function endNightmare(){
  nightmare
    .halt();
  // Report as not busy

  debug("####### nightmare halted: ",nightmare.running);
  nightmare = Nightmare(nightmareOptions);
  notBusy();
  debug("####### nightmare restarted: ",nightmare.running);

} // END endNightmare



/** ########################################################
 *  # DCM Nightmare:
 ######################################################## */

// Nightmare prototype extension and instantiation for DCM
//Nightmare.action(methods.nameSpace, methods.actionsObject);

// Nightmare options
nightmareOptions = {}
if      (env.NODE_ENV === "development") nightmareOptions = { show: true, width: 1280, height: 1024 }
else if (env.NODE_ENV === "staging")     nightmareOptions = { show: false }
else if (env.NODE_ENV === "production")  nightmareOptions = { show: false }
let nightmareTimeouts = {
  waitTimeout      : RATE_LIMITS.nmTimeouts.waitTimeout,
  gotoTimeout      : RATE_LIMITS.nmTimeouts.gotoTimeout,
  loadTimeout      : RATE_LIMITS.nmTimeouts.loadTimeout,
  executionTimeout : RATE_LIMITS.nmTimeouts.executionTimeout
} // END nightmareTimeouts

let proxy = {
  switches: {
    'proxy-server': '163.172.48.109:15001',
    'ignore-certificate-errors': true
  }
} // END

Object.assign(nightmareOptions, nightmareTimeouts);
// Nightmare init
let nightmare = Nightmare(nightmareOptions);
debug("nightmareOptions",nightmareOptions);


/** ########################################################
 *  # DCM scrapping routines:
 ######################################################## */


/**
 * Will get all categories from home
 */
var portal_discovery = exports.portal_demo = () => {
  return new Promise(function(resolve, reject){

    // Login
    rateLimit(() => {

      // Proxi test
      nightmare
        .goto('http://yahoo.com')
        .wait('#home-layout')
        .evaluate( _ => {
          var test = $(".tituloslogan").text(),
              ip;

          $.ajax({url: "https://api.ipify.org?format=json", success: function(result){
            ip = result.ip
          }});

          return {
            text: text,
            ip: ip
          }; // END ret

        })
        .then(categoryTree => traverseSubCategories(categoryTree));

    }); // END rateLimit

  }); // END promise

} // END portal_discovery







