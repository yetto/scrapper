const
  debug              = require('../libs/debugConf').log('dcm:actions'),
  debugErr           = require('../libs/debugConf').err('dcm:actions'),
  Nightmare   = require('nightmare'),
  validator   = require('validator'),
  jquery      = require('jquery'),
  Promise     = require("bluebird");

exports.nameSpace     = 'dcm';
exports.actionsObject = {


  getUpc: function (done) {
    //debug(' === dcm_actions > getUpc');
    this.evaluate_now(function () {
      return $('#foto_producto1 tbody tr:nth-child(3)').text().trim().replace("UPC: ","");
    }, done);

  }, // END getUpc


  /**
   * getCategorie: get categories from DCM homepage menu.
   *
   */
  getCategories: function (done) {
    //debug(' === dcm_actions > getCategories');
    this.evaluate_now(function () {
      // Category Tree
      var categoryLinks = [],
          menuLabels    = ["oficina",
                            "papel",
                            "tintasytoners",
                            "limpieza",
                            "cafeteria",
                            "tecnologia"],
          menuElements   = [".dropdown_oficina",
                            ".dropdown_papel",
                            ".dropdown_tintasytoners",
                            ".dropdown_limpieza",
                            ".dropdown_cafeteria",
                            ".dropdown_tecnologia"];
      menuElements.forEach((element, x) => {
        links = $(element + " li a");
        links.each((y) => {
          categoryLinks.push({
            url : links[y].href,
            parentCategory : menuLabels[x],
            childCategory : links[y].innerText
          });
        });
      });
      return categoryLinks;
    }, done);

  }, // END getCategories



  /**
   * getPages: gets pages from a DCM category page
   *
   */
  getPages: function (done) {
    //debug(' === dcm_actions > getPages');
    this.evaluate_now(function () {
      var pagesAnchors = [],
          pages        = $("#paginas a");
      pages.each(function(index){
        // Do not push "Next"
        if ($(pages[index]).text().trim() !== "Siguiente") pagesAnchors.push(pages[index].href);
      });
      return pagesAnchors;
    }, done);

  }, // END getPages



  /**
   * getProductLinks: gets products links from a category DCM page.
   *
   */
  getProductLinks: function (done) {
    //debug(' === dcm_actions > getProductLinks');
    this.evaluate_now(function () {
      var productMeta  = [],
          products     = $("#caja_fotoproducto_outlet a:nth-child(1)"),
          upcs         = $("#foto_producto1 tr:nth-child(3)"),
          descriptions = $(".espacio_descripcionproducto tr:nth-child(3)"),
          exist        = ($(".azulito h4").length === 0 ? true : false);

      if (products.length === 0 && !exist) productMeta.push({ exist: exist });
      else {
        products.each(function(index){
          productMeta.push({
            url         : products[index].href,
            upc         : upcs[index].innerText.replace("UPC:","").trim(),
            description : descriptions[index].innerText,
            exist       : exist

          });

        });

      } // EMD if
      return productMeta;

    }, done);

  }, // END getPages



  /**
   * getProductInfo: gets all product info from a DCM product page.
   * @param {Object} contextInfo: should contaitn: { category, subCategory }
   */
  getProductInfo: function (contextInfo, done) {
    //debug(' === dcm_actions > getProductInfo');
    this.evaluate_now((contextInfo) => {

      if (!contextInfo) contextInfo = {};

      /* Attributes */
      var foundAttributes = {};
      var attributesNames = $("#especificaciones_total_articulo td:nth-child(1):not(td[colspan=2])")
      attributesNames.each(function(index){
        foundAttributes[$(this).text().replace(':','').replace('.','').trim()] = "";
      });
      var attributesValues = $("#especificaciones_total_articulo td:nth-child(2)")
      var attrKeys = Object.keys(foundAttributes);
      attributesValues.each(function(index){
        foundAttributes[attrKeys[index]] = $(this).text().trim();
      });

      // Images
      var foundImages = [];
      var pageImages = $('.large_image');
      pageImages.each(function(index){
        foundImages.push({ url : pageImages[index].src });
      });

      // Stock
      var stock = {};
      var where = $("#cajitaCarrito td:nth-child(2)");
      var quantity = $("#cajitaCarrito td:nth-child(3)");
      where.each(function(index){
        if (index !== 0 && index !== 1 && parseInt($(quantity[index]).text())) {
          stock[$(this).text().trim()] = parseInt($(quantity[index]).text());
        }
      });

      var currency = "MXN";
      var price = $("#1unitarioc1").text().replace(',','').replace(/^\s+|\s+$|\s+(?=\s)/g, "").replace(/\$/g,'');
      if (price.indexOf("US") !== -1) {
        price = price.replace(/US/g,'');
        currency = "US";
      }; // END if

      var data = {
        name        : $('.estilo_tit_especificacionesarticulo').text().trim(),
        price       : price,
        brand       : foundAttributes["Marca"] || null,
        provider    : "dcm.com.mx",
        providerSku : window.document.title.replace(/DC Mayorista ::: Ficha Tecnica: /g,''),
        globalSku   : null,
        partNumber  : foundAttributes["Número de parte fabricante"] || foundAttributes["No. de parte"] || null,
        images      : foundImages,
        attributes  : foundAttributes,
        currency    : currency,
        stock       : stock,
        _scrapeDate : Date.now(),
      };

      if (Object.keys(contextInfo).length > 0) {
        data.productUpc  = contextInfo.upc,
        data.description = contextInfo.contextInfo,
        data.category    = contextInfo.category || null,
        data.subCategory = contextInfo.subCategory || null,
        data._lastStatus = contextInfo.statusCode,
        data._sourceUrl  = contextInfo.url || null
      }

      return data;

    }, done, contextInfo);

  } // END getProductInfo

} // END exports.actionsObject