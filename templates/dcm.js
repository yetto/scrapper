
const
  debug             = require('../libs/debugConf').log('dcm:template'),
  debugErr          = require('../libs/debugConf').err('dcm:template'),
  RateLimiter       = require('limiter').RateLimiter,
  validator         = require('validator'),
  Promise           = require("bluebird"),
  co                = require("co"),
  // DCM helpers
  helpers           = require("./dcm_helpers"),
  methods           = require("./dcm_actions"),
  // Libs
  nightmareWrapper  = require('../libs/nightmareWrapper'),
  // DCM reports
  templateReports   = require("../reports/dcm_reports"),
  reportProduct     = require("../reports/product"),
  reports           = require("../reports/product"),
  { updateCrawler } = require('../reports/crawler'),
  // Constants
  { NODE_ENV,
    RATE_LIMITS }   = require('../constants/constants'),
  limiter           = new RateLimiter(RATE_LIMITS.howMany, RATE_LIMITS.every);


/*

product:controller 29858 Error in upsert operation: product. key No. perforaciones must not contain '.' +11ms
product:controller 29858 Error: key No. perforaciones must not contain '.'


crawler:report  ~~~ Updating Scraping Order > ID:  59726d2fe05d5d74ae25e0f8  Status:  Error: Evaluation timed out after 25000msec.  Are you calling done() or resolving your promises?
  at Timeout._onTimeout (/home/ytt/Repos/Caro/node_modules/nightmare/lib/actions.js:509:10)


-- Filter out . and : from attributes

dcm:template  === Traversing:  Perforadoras y repuestos  Index:  0  Total:  1 +9s
dcm:template { message: 'navigation error',
dcm:template   code: -7,
dcm:template   details: 'Navigation timed out after 25000 ms',
dcm:template   url: 'http://dcm.com.mx/catalogo/preArticulos.asp?flag=25&arbol=S&lineaNeg=PRO&fami=4&arSubFam=47' } +

dcm:template $ is not defined +2s

*/



/**
 * Messages
 */
let errors = {
  login : "Something went wrong when loogin into DCM"
}; // END errors



/** ########################################################
 *  # DCM Nightmare:
 ######################################################## */
nightmareWrapper.hookActions(methods);
let nightmare;

// DCM data
let options = {
  login : true,
  loginData : {
    user : "93984",
    pass : "B8638488",
    key  : "93984"
  }
}


/** ########################################################
 *  # DCM scrapping routines:
 ######################################################## */


/**
 * Will get all categories from home
 */
var dcm_discovery = exports.dcm_discovery = () => {
  nightmare = nightmareWrapper.start();

  return new Promise(function(resolve, reject){
    nightmare
      .use(helpers.login(options.loginData))
      .then(loggedIn => getHomeCats(loggedIn))
      .then(categoryTree => {
        nightmareWrapper.kill(nightmare);
        debug(" === dcm_discovery > DONE > categoryTree: ", categoryTree.length);
        resolve(categoryTree);
        templateReports.dcmDiscovery(categoryTree);

      }) // END then
      .catch(error => {
        debugErr(" === dcm_discovery > error: ",error.message);
        nightmareWrapper.kill(nightmare);
        reject(error);

      });

  }); // END promise

} // END dcm_discovery



/**
 * Traverse array of categories
 * @categories: Array of categories
 */
var dcm_category = exports.dcm_category = (categories) => {
  nightmare = nightmareWrapper.start();

  return new Promise(function(resolve, reject){
    nightmare
      .use(helpers.login(options.loginData))
      .then(loggedIn => traverseSubCategories(categories))
      .then(products  => {
        nightmareWrapper
          .kill(nightmare)
          .then(() => {
            debug(" === dcm_category > DONE > products",products);
            resolve(products);

          })
          .catch(error => reject(error));

      }) // END then
      .catch(error => {
        nightmareWrapper
          .kill(nightmare)
          .then(() => {
            debug(" === dcm_category > error: ",error);
            reject(error);

          })
          .catch(error => reject(error));

      });

  }); // END promise

} // END dcm_category



/**
 * Scrape product information
 * @products: Array of products
 */
var dcm_products = exports.dcm_product = (product) => {
  debug(" === dcm_product > ", product);
  nightmare = nightmareWrapper.start();
  let productInfo,
      templateCode = "dcpro";

  product.templateCode = templateCode;

  return new Promise(function(resolve, reject){
    nightmare
      .use(helpers.login(options.loginData))
      .use(helpers.search(product.providerSku))
      .dcm.getProductLinks()
      .then(productLink => {
        let productInfo = productLink[0];
        debug(" === dcm_product > ", product.providerSku, " > Exist: ", productInfo.exist);

        if(!productInfo.exist) {
          product.code = "2";
          reportProduct(product)
            .catch(error => debugErr(" === dcm_product > reportProduct error: ", error.error.code));

          let err = new Error("DCM product does not exist.");
          reject(err);
          throw err;

        } else {// END if

          return nightmare
            .goto(productInfo.url)
            .wait('#contenedor_especificacionesarticulo')
            .dcm.getProductInfo(productInfo);

        }

      })
      .then(info => {
        productInfo = info;
        productInfo.code = 1;
        return nightmareWrapper
          .kill(nightmare);

      })
      .then( _ => reportProduct(productInfo))
      .then( _ => resolve(" === dcm_products > DONE > ", productInfo.providerSku))
      .catch(error => {

        if(error.message.indexOf('.wait() timed out after') != -1) {
          product.code = "3";
          reportProduct(product)
            .catch(error => debugErr(" === dcm_product > reportProduct error: ", error.error.code));

        }

        nightmareWrapper
          .kill(nightmare)
          .then(() => {
            debugErr(" === dcm_product > nightmare KILL > error: ", error.message);
            reject(error);

          }) // END nightmareWrapper

      }); // END Catch

  }); // END promise

} // END dcm_products




/** ########################################################
 *  # DCM scrapping routine wrappers:
 ######################################################## */


// Get categories from home
function getHomeCats(){
  return new Promise(function(resolve, reject){
    nightmare
      .wait("#new_tipodecambio")
      .dcm.getCategories()
      .then(categoryTree => resolve(categoryTree))
      .catch(error => {
        debugErr(error);
        reject(error);
      });

  }); // END promise
} // END getHomeCats





// Get pagin links from category pages
function traverseSubCategories(categoryTree){
  return new Promise(function(resolve, reject){

    function *traverseSubCategoriePages() {
      let index = 0;
      while(index + 1 <= categoryTree.length){
        debug(" === Traversing: ",categoryTree[index].childCategory," Index: ",index," Total: ",categoryTree.length);
        yield scrapeSubCategory(categoryTree[index]);
        index++;
      } // END while
    } // END traverseSubCategoriePages

    co(traverseSubCategoriePages())
      .then(data => {
        resolve(data);

      })
      .catch(error => {
        debugErr(" === traverseSubCategoriePages > Error: ",error);
        resolve();

      });

  }); // END promise
} // END traverseSubCategories



// Get pagin links from category pages
function scrapeSubCategory(subCategory){
  return new Promise(function(resolve, reject){

    let pageLinks    = [],
        productLinks = [];

    nightmare
      .goto(subCategory.url)
      .wait('#ordenar_busqueda')
      .dcm.getPages()
      .then(pagesLinks => {
        pageLinksScrapped(pagesLinks, subCategory)
        if (pagesLinks.length === 0) resolve(pagesLinks);
        crawlPages();

      })
      .catch(error => {
        // DEAL WITH ERROR 7, error 7 will kill operations if it happens here >>> HERE <<<
        // Reject order, mark as error
        // IDEA, you could probably retry and then kill it after max try

        // DEAL WITH: $ is not defined >>> HERE <<<
        nightmareWrapper
          .kill(nightmare)
          .then(() => {
            debugErr(" === scrapeSubCategory > error: ",error);
            reject(error);

          }) // END nightmareWrapper

      }); // END nightmare

    function *traverseSubCategory() {
      // loop indexes
      let pageN    = 0,
          productN = 0;

      /* Visit all subcategory pages and get all product links */
      while(pageN + 1 <= pageLinks.length){
        if (pageLinks[pageN].url !== undefined) {
          let { url, parentCategory, childCategory } = pageLinks[pageN];

          yield nightmare
            .goto(url)
            .wait('#ordenar_busqueda')
            .dcm.getProductLinks()
            .then(productLinks => productLinksScrapped(productLinks, subCategory))
            .catch(error => {
              debugErr(" === *traverseSubCategory while#1 > error: ",error);
              //resolve(error);

            }); // END nightmare

        } // END while

        pageN++;

      } // END while

      /* Scrape product links from category */
      while(productN + 1 <= productLinks.length){

        if (productLinks[productN].url !== undefined) {
          let { url, parentCategory, childCategory, description, upc  } = productLinks[productN],
                pageInfo;

          yield nightmare
            .goto(url)
            .then(function(response) {
              pageInfo = response;
              return nightmare
                .wait('#contenedor_especificacionesarticulo')
                .dcm.getProductInfo({
                  url : pageInfo.url,
                  description : description,
                  upc : upc,
                  category : parentCategory || null,
                  subCategory : childCategory || null,
                  statusCode : pageInfo.code

                });

            })
            .then(productInfo => reportProduct(productInfo))
            .catch(error => {
              // Deal with: { StatusCodeError: 400 - {"message":"The server cannot or will not process the request due to something that is perceived to be a client error.","code":400,"cue":"Double check provided information. A validation error ocurred.","context":"Error in upsert operation: product."} >>>> HERE <<<<

              // Deal with: === *traverseSubCategory while#2 > error:  { message: 'navigation error', code: -7, >>>> HERE <<<<

              debugErr(" === *traverseSubCategory while#2 > error: ", error.message || error);

            }); // END nightmare

        } // END if

        productN++;

      } // END while

    } // END traverseSubCategory

    function crawlPages() {
      co(traverseSubCategory())
        .then(data => resolve(data))
        .catch(error => {
          debugErr(" === crawlPages > error: ", error.message || error);
          reject(error);

        });

    } // END crawlPages


    /** ########################################################
     *  # HELPERS
     ######################################################## */


    /**
     * Format scrapped page links
     */
    function pageLinksScrapped(links, meta){
      links.forEach(link => pageLinks.push({
        url : link,
        parentCategory : meta.parentCategory,
        childCategory : meta.childCategory
      }));
      if (pageLinks.length > 0) debug(" === pageLinksScrapped #: ",pageLinks.length," URL: ",pageLinks[0].url);

    } // END pageLinksScrapped


    /**
     * Format scrapped product page links
     */
    function productLinksScrapped(productMeta, subcategoryMeta){
      productMeta.forEach(meta => productLinks.push({
        url : meta.url,
        upc : meta.upc,
        description : meta.description,
        parentCategory : subcategoryMeta.parentCategory,
        childCategory : subcategoryMeta.childCategory
      }));
      debug(" === scrapeSubCategory > productLinksScrapped: ",productLinks.length);

    } // END productLinksScrapped


  }); // END promise

} // END scrapeSubCategory


// Get product info from product pages
function traverseProductPage(productInfo){
  debug(" === traverseProductPage > ",productInfo)
  return new Promise(function(resolve, reject){

    let productScrapped = (product) => {
      debug(" === traverseProductPage > productScrapped: ",product);
    } // END productScrapped

    let scrapeProductInfo = function() {
      debug(" === scrapeProductInfo: ",productInfo);
      nightmare
        .goto(productInfo.url)
        .wait('#contenedor_especificacionesarticulo')
        .dcm.getProductInfo({
          category : productInfo.parentCategory,
          subCategory : productInfo.childCategory
        })
        .then(product => productScrapped(product))
        .catch(error => {
          debugErr(" === traverseProductPage > error: ",error);
          //resolve(error);
        });
    } // END scrapeProductInfo

  }); // END promise

} // END traverseProductPage